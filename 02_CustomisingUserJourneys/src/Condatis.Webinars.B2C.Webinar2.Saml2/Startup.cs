// <copyright file="Startup.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar2.Saml2
{
    using System;
    using System.Security.Cryptography.X509Certificates;
    using Condatis.Webinars.B2C.Webinar2.Saml2.Models;
    using Condatis.Webinars.B2C.Webinar2.Saml2.Routing;
    using Microsoft.AspNetCore.Authentication.Cookies;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc.Infrastructure;
    using Microsoft.AspNetCore.Rewrite;
    using Microsoft.Azure.KeyVault;
    using Microsoft.Azure.Services.AppAuthentication;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Sustainsys.Saml2;
    using Sustainsys.Saml2.AspNetCore2;
    using Sustainsys.Saml2.Metadata;
    using Sustainsys.Saml2.Saml2P;
    using Saml2Options = Condatis.Webinars.B2C.Webinar2.Saml2.Models.Saml2Options;

    /// <summary>
    /// Startup.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="configuration">Configuration.</param>
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        /// <summary>
        /// Configures services.
        /// </summary>
        /// <param name="services">Service collection.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<SiteOptions>(this.Configuration);

            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            services.AddApplicationInsightsTelemetry();
            services.AddMiniProfiler();

            services.AddAuthentication(opt =>
            {
                opt.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                opt.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                opt.DefaultChallengeScheme = Saml2Defaults.Scheme;
            })
            .AddSaml2(Saml2Defaults.Scheme, opt =>
            {
                var azureServiceTokenProvider = new AzureServiceTokenProvider();
                using var keyVaultClient = new KeyVaultClient(new KeyVaultClient.AuthenticationCallback(azureServiceTokenProvider.KeyVaultTokenCallback));

                var saml2Options = new Saml2Options();
                this.Configuration.Bind(Saml2Defaults.Scheme, saml2Options);

                opt.SPOptions.EntityId = new EntityId(saml2Options.EntityId);
                opt.SPOptions.ModulePath = "/saml";
                opt.SPOptions.ReturnUrl = saml2Options.ReturnUrl;
                opt.SPOptions.AuthenticateRequestSigningBehavior = saml2Options.AuthenticateRequestSigningBehavior;
                opt.SPOptions.NameIdPolicy = new Saml2NameIdPolicy(null, saml2Options.NameIdFormat);
                opt.SPOptions.WantAssertionsSigned = saml2Options.WantAssertionsSigned;

                foreach (var identityProvider in saml2Options.IdentityProviders)
                {
                    opt.IdentityProviders.Add(new IdentityProvider(new EntityId(identityProvider.EntityId), opt.SPOptions)
                    {
                        MetadataLocation = identityProvider.MetadataLocation,
                        LoadMetadata = true,
                        AllowUnsolicitedAuthnResponse = identityProvider.AllowUnsolicitedAuthnResponse,
                    });
                }

                var serviceCert = keyVaultClient.GetSecretWithHttpMessagesAsync(this.Configuration["KeyVaultUrl"], saml2Options.ServiceCertificateName, saml2Options.ServiceCertificateVersion)
                                                .Result;

                opt.SPOptions.ServiceCertificates.Add(new X509Certificate2(Convert.FromBase64String(serviceCert.Body.Value)));
            })
            .AddCookie();

            services.ConfigureApplicationCookie(opt =>
            {
                opt.Cookie.SameSite = SameSiteMode.None;
            });

            services.AddHealthChecks();

            services.AddControllersWithViews();
            services.AddRouting(opt =>
            {
                opt.LowercaseUrls = true;
                opt.LowercaseQueryStrings = false;
                opt.ConstraintMap["slugify"] = typeof(SlugifyParameterTransformer);
            });
        }

        /// <summary>
        /// Configures the middleware pipeline.
        /// </summary>
        /// <param name="app">App builder.</param>
        /// <param name="env">Web host environment.</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");

                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCsp(opts =>
            {
                opts.DefaultSources(s => s.Self())
                    .ScriptSources(s => s.Self().CustomSources("https://stackpath.bootstrapcdn.com", "https://cdnjs.cloudflare.com"))
                    .StyleSources(s => s.Self().CustomSources("https://stackpath.bootstrapcdn.com", "https://cdnjs.cloudflare.com", "https://fonts.googleapis.com").UnsafeInline())
                    .ImageSources(s => s.Self().CustomSources("https://stackpath.bootstrapcdn.com", "https://condatis.com"))
                    .FontSources(s => s.Self().CustomSources("https://stackpath.bootstrapcdn.com", "https://cdnjs.cloudflare.com", "https://fonts.gstatic.com"))
                    .FrameSources(s => s.None())
                    .UpgradeInsecureRequests();
            });

            app.UseXContentTypeOptions();
            app.UseReferrerPolicy(opts => opts.NoReferrer());

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseXXssProtection(opt => opt.EnabledWithBlockMode());
            app.UseNoCacheHttpHeaders();
            app.UseXfo(xfo => xfo.Deny());
            app.UseRedirectValidation(opts =>
            {
                opts.AllowSameHostRedirectsToHttps();
                opts.AllowedDestinations(this.Configuration["AzureAdB2CDomain"]);
            });

            app.UseAuthentication();

            if (env.IsDevelopment())
            {
                app.UseMiniProfiler();
            }

            var rewriteOptions = new RewriteOptions();
            rewriteOptions.Rules.Add(new RedirectLowercaseRule());
            app.UseRewriter(rewriteOptions);

            app.UseRouting();
            app.UseEndpoints(e =>
            {
                e.MapControllerRoute("Default", "{controller:slugify=Home}/{action:slugify=Index}/{id?}");
                e.MapHealthChecks("/health");
            });
        }
    }
}
