﻿// <copyright file="Saml2Options.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar2.Saml2.Models
{
    using System;
    using System.Collections.Generic;
    using Sustainsys.Saml2.Configuration;
    using Sustainsys.Saml2.Saml2P;

    /// <summary>
    /// Provides SAML2 SP options.
    /// </summary>
    public class Saml2Options
    {
        /// <summary>
        /// Gets or sets the Entity Id.
        /// </summary>
        public string EntityId { get; set; }

        /// <summary>
        /// Gets or sets the return URL.
        /// </summary>
        public Uri ReturnUrl { get; set; }

        /// <summary>
        /// Gets or sets the minimum supported signing algorithm for incoming responses.
        /// </summary>
        public string MinIncomingSigningAlgorithm { get; set; }

        /// <summary>
        /// Gets or sets the signing request behaviour.
        /// </summary>
        public SigningBehavior AuthenticateRequestSigningBehavior { get; set; } = SigningBehavior.IfIdpWantAuthnRequestsSigned;

        /// <summary>
        /// Gets or sets the NameId format.
        /// </summary>
        public NameIdFormat NameIdFormat { get; set; } = NameIdFormat.Unspecified;

        /// <summary>
        /// Gets or sets whether assertions should be signed.
        /// </summary>
        public bool WantAssertionsSigned { get; set; } = true;

        /// <summary>
        /// Gets or sets the IdPs this SP can interact with.
        /// </summary>
        public IEnumerable<Saml2IdentityProvider> IdentityProviders { get; set; }

        /// <summary>
        /// Gets or sets the service certificate name in Key Vault.
        /// </summary>
        public string ServiceCertificateName { get; set; }

        /// <summary>
        /// Gets or sets the service certificate version.
        /// </summary>
        public string ServiceCertificateVersion { get; set; }
    }
}
