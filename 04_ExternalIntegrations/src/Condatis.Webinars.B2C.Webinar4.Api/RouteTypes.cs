﻿// <copyright file="RouteTypes.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api
{
    /// <summary>
    /// Provides routes for API actions.
    /// </summary>
    public static class RouteTypes
    {
        /// <summary>
        /// Provides routes for API actions.
        /// </summary>
        public static class Default
        {
            /// <summary>
            /// Actions identified by just the controller name (and verb).
            /// </summary>
            public const string Controller = "{controller:slugify}";

            /// <summary>
            /// Actions identified by the controller name and action.
            /// </summary>
            public const string ControllerAction = "{controller:slugify}/{action:slugify}";

            /// <summary>
            /// Actions identified by just the controller name (and verb), for a particular record.
            /// </summary>
            public const string ControllerId = "{controller:slugify}/{id}";
        }

        /// <summary>
        /// Provides routes for API actions used by B2C.
        /// </summary>
        public static class B2C
        {
            /// <summary>
            /// Actions identified by just the controller name (and verb).
            /// </summary>
            public const string Controller = "b2c/" + RouteTypes.Default.Controller;

            /// <summary>
            /// Actions identified by the controller name and action.
            /// </summary>
            public const string ControllerAction = "b2c/" + RouteTypes.Default.ControllerAction;

            /// <summary>
            /// Actions identified by just the controller name (and verb), for a particular record.
            /// </summary>
            public const string ControllerId = "b2c/" + RouteTypes.Default.ControllerId;
        }
    }
}
