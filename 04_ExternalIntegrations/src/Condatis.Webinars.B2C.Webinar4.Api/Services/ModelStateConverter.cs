﻿// <copyright file="ModelStateConverter.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Services
{
    using System;
    using System.Linq;
    using System.Text.Json.Serialization;
    using Condatis.Webinars.B2C.Webinar4.Api.Models;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.ModelBinding;

    /// <inheritdoc />
    public class ModelStateConverter : IModelStateConverter
    {
        private readonly IVersionNumberAccessor versionNumberAccessor;
        private readonly IHttpContextAccessor httpContextAccessor;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModelStateConverter"/> class.
        /// </summary>
        /// <param name="versionNumberAccessor">Version number accessor.</param>
        /// <param name="httpContextAccessor">HTTP Context accessor.</param>
        public ModelStateConverter(IVersionNumberAccessor versionNumberAccessor, IHttpContextAccessor httpContextAccessor)
        {
            this.versionNumberAccessor = versionNumberAccessor;
            this.httpContextAccessor = httpContextAccessor;
        }

        /// <inheritdoc />
        public B2CErrorResponse ToErrorResponse(Type modelType, ModelStateDictionary modelState)
        {
            return this.ToErrorResponse(null, modelType, modelState);
        }

        /// <inheritdoc />
        public B2CErrorResponse ToErrorResponse(string userMessage, Type modelType, ModelStateDictionary modelState)
        {
            var errorFields = modelState.Where(e => e.Value.ValidationState == ModelValidationState.Invalid)
                                        .Select(e => GetMessage(e.Key, e.Value, modelType));

            var developerMessage = string.Join("\r\n", errorFields);

            return new B2CErrorResponse
            {
                Version = this.versionNumberAccessor.GetVersionNumber(),
                DeveloperMessage = developerMessage,
                UserMessage = userMessage ?? developerMessage,
                RequestId = this.httpContextAccessor.HttpContext.TraceIdentifier,
            };
        }

        private static string GetMessage(string modelStateKey, ModelStateEntry modelStateEntry, Type modelType)
        {
            var errorMessage = string.Join("\r\n", modelStateEntry.Errors.Select(e => e.ErrorMessage));

            if (string.IsNullOrWhiteSpace(modelStateKey))
            {
                return errorMessage;
            }

            return $"{GetPropertyName(modelStateKey, modelType)} is invalid: {errorMessage}";
        }

        private static string GetPropertyName(string key, Type modelType)
        {
            if (modelType == null)
            {
                return key;
            }

            var property = modelType.GetProperties().FirstOrDefault(p => p.Name.Equals(key, StringComparison.InvariantCultureIgnoreCase));
            if (property == null)
            {
                return key;
            }

            var jsonPropertyName = property.GetCustomAttributes(typeof(JsonPropertyNameAttribute), true).Cast<JsonPropertyNameAttribute>().FirstOrDefault();
            if (!string.IsNullOrWhiteSpace(jsonPropertyName?.Name))
            {
                return jsonPropertyName.Name;
            }

            var formPropertyName = property.GetCustomAttributes(typeof(BindPropertyAttribute), true).Cast<BindPropertyAttribute>().FirstOrDefault();
            if (!string.IsNullOrWhiteSpace(formPropertyName?.Name))
            {
                return formPropertyName.Name;
            }

            return key;
        }
    }
}
