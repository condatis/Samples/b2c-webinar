﻿// <copyright file="Startup.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar3.OpenIDConnect
{
    using System.Threading.Tasks;
    using Condatis.Webinars.B2C.Webinar3.OpenIDConnect.Models;
    using Condatis.Webinars.B2C.Webinar3.OpenIDConnect.Routing;
    using Condatis.Webinars.B2C.Webinar3.OpenIDConnect.Services.Authorization;
    using Microsoft.AspNetCore.Authentication.OpenIdConnect;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc.Infrastructure;
    using Microsoft.AspNetCore.Rewrite;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Identity.Web;
    using Microsoft.Identity.Web.UI;
    using Microsoft.IdentityModel.Logging;

    /// <summary>
    /// Startup.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="configuration">Configuration.</param>
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        /// <summary>
        /// Configures services.
        /// </summary>
        /// <param name="services">Service collection.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<SiteOptions>(this.Configuration);
            services.Configure<CookiePolicyOptions>(opt =>
            {
                opt.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            services.AddApplicationInsightsTelemetry();
            services.AddMiniProfiler();

            services.AddAuthentication(OpenIdConnectDefaults.AuthenticationScheme)
                    .AddMicrosoftIdentityWebApp(
                    opt =>
                    {
                        this.Configuration.Bind("AzureAdB2C", opt);
                        opt.Events.OnRedirectToIdentityProvider += ctx =>
                        {
                            // HACK: Microsoft.Identity.Web doesn't handle code flow correctly for non-default policies
                            if (ctx.Properties.Items.TryGetValue("response_type", out var responseType))
                            {
                                ctx.ProtocolMessage.ResponseType = responseType;
                            }

                            return Task.CompletedTask;
                        };
                    },
                    opt =>
                    {
                        opt.Cookie.SameSite = SameSiteMode.None;
                        opt.Cookie.SecurePolicy = CookieSecurePolicy.Always;
                        opt.Cookie.IsEssential = true;
                    });

            services.AddAuthorization(opt =>
            {
                opt.AddPolicy(PolicyNames.LoA1, p =>
                {
                    p.RequireAuthenticatedUser();
                });
                opt.AddPolicy(PolicyNames.LoA2, p =>
                {
                    p.RequireAuthenticatedUser()
                     .RequireClaim("loa", "2");
                });
            });

            services.AddHealthChecks();

            var mvcBuilder = services.AddControllersWithViews()
                                     .AddMicrosoftIdentityUI();
#if DEBUG
            mvcBuilder.AddRazorRuntimeCompilation();
#endif

            services.AddRouting(opt =>
            {
                opt.LowercaseUrls = true;
                opt.LowercaseQueryStrings = false;
                opt.ConstraintMap["slugify"] = typeof(SlugifyParameterTransformer);
            });
        }

        /// <summary>
        /// Configures the middleware pipeline.
        /// </summary>
        /// <param name="app">App builder.</param>
        /// <param name="env">Web host environment.</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                IdentityModelEventSource.ShowPII = true;
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");

                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCsp(opts =>
            {
                opts.DefaultSources(s => s.Self())
                    .ScriptSources(s => s.Self().CustomSources("https://stackpath.bootstrapcdn.com", "https://cdnjs.cloudflare.com"))
                    .StyleSources(s => s.Self().CustomSources("https://stackpath.bootstrapcdn.com", "https://cdnjs.cloudflare.com", "https://fonts.googleapis.com").UnsafeInline())
                    .ImageSources(s => s.Self().CustomSources("https://stackpath.bootstrapcdn.com", "https://condatis.com"))
                    .FontSources(s => s.Self().CustomSources("https://stackpath.bootstrapcdn.com", "https://cdnjs.cloudflare.com", "https://fonts.gstatic.com"))
                    .FrameSources(s => s.None())
                    .UpgradeInsecureRequests();
            });

            app.UseXContentTypeOptions();
            app.UseReferrerPolicy(opts => opts.NoReferrer());

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseXXssProtection(opt => opt.EnabledWithBlockMode());
            app.UseNoCacheHttpHeaders();
            app.UseXfo(xfo => xfo.Deny());
            app.UseRedirectValidation(opts =>
            {
                opts.AllowSameHostRedirectsToHttps();
                opts.AllowedDestinations(this.Configuration["AzureAdB2C:Instance"]);
            });

            if (env.IsDevelopment())
            {
                app.UseMiniProfiler();
            }

            var rewriteOptions = new RewriteOptions();
            rewriteOptions.Rules.Add(new RedirectLowercaseRule());
            app.UseRewriter(rewriteOptions);

            app.UseRouting();

            app.UseAuthentication();
            app.UseMiddleware<AuthorizationRoutingMiddleware>();
            app.UseAuthorization();
            app.UseEndpoints(e =>
            {
                e.MapControllerRoute("Default", "{controller:slugify=Home}/{action:slugify=Index}/{id?}");
                e.MapHealthChecks("/health");
            });
        }
    }
}
