﻿// <copyright file="AuthServiceCollectionExtensions.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Microsoft.Extensions.DependencyInjection
{
    using Condatis.Webinars.B2C.Webinar4.Api;

    /// <summary>
    /// Authorization extensions for <see cref="IServiceCollection"/>.
    /// </summary>
    public static class AuthServiceCollectionExtensions
    {
        /// <summary>
        /// Adds authorization policies.
        /// </summary>
        /// <param name="services">Services.</param>
        /// <returns>The <see cref="IServiceCollection"/>.</returns>
        public static IServiceCollection AddAuthorizationPolicies(this IServiceCollection services)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicy(PolicyNames.Authorised, p =>
                {
                    p.AddAuthenticationSchemes("Bearer")
                     .RequireAuthenticatedUser();
                });

                options.AddPolicy(PolicyNames.ReadUserGroups, p =>
                {
                    p.Combine(options.GetPolicy(PolicyNames.Authorised))
                     .RequireRole(RoleNames.UserGroupsRead);
                });
                options.AddPolicy(PolicyNames.WriteUser, p =>
                {
                    p.Combine(options.GetPolicy(PolicyNames.Authorised))
                     .RequireRole(RoleNames.UserWrite);
                });

                options.AddPolicy(PolicyNames.LegacyUsers, p =>
                {
                    p.Combine(options.GetPolicy(PolicyNames.Authorised))
                     .RequireRole(RoleNames.UserWrite);
                });
            });

            return services;
        }
    }
}
