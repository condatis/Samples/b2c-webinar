﻿// <copyright file="B2CException.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Services
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents an error thrown by the API that should be communicated to B2C.
    /// </summary>
    [Serializable]
    public class B2CException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="B2CException"/> class.
        /// </summary>
        public B2CException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="B2CException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public B2CException(string message)
            : this(message, message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="B2CException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        /// <param name="userMessage">A user-presentable message that describes the error.</param>
        public B2CException(string message, string userMessage)
            : base(message)
        {
            this.UserMessage = userMessage;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="B2CException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        /// <param name="inner">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner exception is specified.</param>
        public B2CException(string message, Exception inner)
            : this(message, message, inner)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="B2CException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        /// <param name="userMessage">A user-presentable message that describes the error.</param>
        /// <param name="inner">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner exception is specified.</param>
        public B2CException(string message, string userMessage, Exception inner)
            : base(message, inner)
        {
            this.UserMessage = userMessage;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="B2CException"/> class.
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo"/> that holds the serialized object data about the exception being thrown.</param>
        /// <param name="context">The <see cref="StreamingContext"/> that contains contextual information about the source or destination.</param>
        protected B2CException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        /// <summary>
        /// Gets the user-presentable message that describes the error.
        /// </summary>
        public string UserMessage { get; }
    }
}
