﻿// <copyright file="ValidatePasswordRequest.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Models.Passwords
{
    /// <summary>
    /// Represents a request to validate a password.
    /// </summary>
    public class ValidatePasswordRequest
    {
        /// <summary>
        /// Gets or sets the password to be validated.
        /// </summary>
        public string Password { get; set; }
    }
}
