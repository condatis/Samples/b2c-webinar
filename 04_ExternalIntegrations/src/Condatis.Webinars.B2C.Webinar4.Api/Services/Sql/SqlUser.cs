﻿// <copyright file="SqlUser.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Services.Sql
{
    using System;

    /// <summary>
    /// Represents a user in SQL Server.
    /// </summary>
    public class SqlUser
    {
        /// <summary>
        /// Gets or sets the user's unique ID.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the user's forename.
        /// </summary>
        public string Forename { get; set; }

        /// <summary>
        /// Gets or sets the user's surname.
        /// </summary>
        public string Surname { get; set; }
    }
}
