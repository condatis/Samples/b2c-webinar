﻿// <copyright file="B2CErrorResponse.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Models
{
    /// <summary>
    /// Represents an error response to an API request from B2C.
    /// </summary>
    public class B2CErrorResponse
    {
        /// <summary>
        /// Gets or sets the current version of the API.
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Gets or sets the HTTP status of the response.
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// Gets or sets a code representing the response.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets a unique ID that represents the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// Gets or sets a user-presentable error message.
        /// </summary>
        public string UserMessage { get; set; }

        /// <summary>
        /// Gets or sets an error message intended for developers; can contain more system-specific information than <see cref="UserMessage"/>.
        /// </summary>
        public string DeveloperMessage { get; set; }

        /// <summary>
        /// Gets or sets more info about the response.
        /// </summary>
        public string MoreInfo { get; set; }
    }
}
