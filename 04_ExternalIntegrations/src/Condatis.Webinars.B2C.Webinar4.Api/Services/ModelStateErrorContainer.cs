﻿// <copyright file="ModelStateErrorContainer.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Services
{
    using Microsoft.AspNetCore.Mvc.ModelBinding;

    /// <summary>
    /// Provides a container for storing errors in a <see cref="ModelStateDictionary"/>.
    /// </summary>
    public class ModelStateErrorContainer : IErrorContainer
    {
        private readonly ModelStateDictionary modelState;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModelStateErrorContainer"/> class.
        /// </summary>
        /// <param name="modelState">Model state.</param>
        public ModelStateErrorContainer(ModelStateDictionary modelState)
        {
            this.modelState = modelState;
        }

        /// <inheritdoc />
        public void Add(string propertyName, string message)
        {
            this.modelState.AddModelError(propertyName, message);
        }
    }
}
