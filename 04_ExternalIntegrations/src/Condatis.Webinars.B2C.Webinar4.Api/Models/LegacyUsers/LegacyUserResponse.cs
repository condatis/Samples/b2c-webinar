﻿// <copyright file="LegacyUserResponse.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Models.LegacyUsers
{
    using System.ComponentModel.DataAnnotations;
    using Condatis.Webinars.B2C.Webinar4.Api.Services.Identity;

    /// <summary>
    /// Represents a response to creating legacy user.
    /// </summary>
    public class LegacyUserResponse
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LegacyUserResponse"/> class.
        /// </summary>
        /// <param name="legacyUser">Legacy user.</param>
        public LegacyUserResponse(LegacyUser legacyUser)
        {
            this.Id = legacyUser.Id;
            this.EmailAddress = legacyUser.Email;
            this.Forename = legacyUser.Forename;
            this.Surname = legacyUser.Surname;
        }

        /// <summary>
        /// Gets or sets the user's password.
        /// </summary>
        [Required]
        public string Id { get; }

        /// <summary>
        /// Gets or sets the user's email address.
        /// </summary>
        [Required]
        public string EmailAddress { get; }

        /// <summary>
        /// Gets or sets the user's forename.
        /// </summary>
        [Required]
        public string Forename { get; }

        /// <summary>
        /// Gets or sets the user's surname.
        /// </summary>
        [Required]
        public string Surname { get; }
    }
}