﻿// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

(function($) {

    $(function() {
        $('#api .divider').remove();

        var $container = $('<div class="col-md-6 col-xl-6"></div>');
        $container.appendTo($('#api').closest('.row'));

        $('#api .social').appendTo($container);

        $('.accountButton').addClass('btn').addClass('btn-block').addClass('btn-square');
    });

})(jQuery);