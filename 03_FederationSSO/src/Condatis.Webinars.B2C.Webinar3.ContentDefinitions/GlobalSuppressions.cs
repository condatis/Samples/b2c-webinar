﻿// <copyright file="GlobalSuppressions.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1055:Uri return values should not be strings", Justification = "Implementing a framework interface", Scope = "type", Target = "Condatis.Webinars.B2C.Webinar3.ContentDefinitions.Services.Web.AbsoluteUrlHelper")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1054:Uri parameters should not be strings", Justification = "Implementing a framework interface", Scope = "type", Target = "Condatis.Webinars.B2C.Webinar3.ContentDefinitions.Services.Web.AbsoluteUrlHelper")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "Framework method", Scope = "member", Target = "~M:Condatis.Webinars.B2C.Webinar3.ContentDefinitions.Startup.Configure(Microsoft.AspNetCore.Builder.IApplicationBuilder,Microsoft.AspNetCore.Hosting.IWebHostEnvironment)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "Framework method", Scope = "member", Target = "~M:Condatis.Webinars.B2C.Webinar3.ContentDefinitions.Startup.ConfigureServices(Microsoft.Extensions.DependencyInjection.IServiceCollection)")]
