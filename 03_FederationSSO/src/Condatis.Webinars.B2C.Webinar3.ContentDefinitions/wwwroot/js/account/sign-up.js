﻿// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

(function($) {

    $(function() {
        $('#password').closest('.attrEntry').before($('<hr class="my-5">'));
        $('#reenterPassword').closest('.attrEntry').after($('<hr class="my-5">'));
        $('#continue').closest('.btn-group').addClass('mt-5');
    });

})(jQuery);