﻿// <copyright file="PasswordResponseErrorContainer.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Services.Passwords
{
    using Condatis.Webinars.B2C.Webinar4.Api.Models.Passwords;

    /// <summary>
    /// Provides a container for adding errors to a <see cref="ValidatePasswordResponse"/>.
    /// </summary>
    public class PasswordResponseErrorContainer : IErrorContainer
    {
        private readonly ValidatePasswordResponse response;

        /// <summary>
        /// Initializes a new instance of the <see cref="PasswordResponseErrorContainer"/> class.
        /// </summary>
        /// <param name="response">Validate password response.</param>
        public PasswordResponseErrorContainer(ValidatePasswordResponse response)
        {
            this.response = response;
        }

        /// <inheritdoc />
        public void Add(string propertyName, string message)
        {
            this.response.AddError(message);
        }
    }
}
