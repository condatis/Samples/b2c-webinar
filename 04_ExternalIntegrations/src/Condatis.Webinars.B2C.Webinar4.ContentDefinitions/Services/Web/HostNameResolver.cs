﻿// <copyright file="HostNameResolver.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.ContentDefinitions.Services.Web
{
    using System.Linq;
    using Microsoft.AspNetCore.Http;

    /// <inheritdoc />
    public class HostNameResolver : IHostNameResolver
    {
        private readonly IHttpContextAccessor httpContextAccessor;

        /// <summary>
        /// Initializes a new instance of the <see cref="HostNameResolver"/> class.
        /// </summary>
        /// <param name="httpContextAccessor">HTTP context accessor.</param>
        public HostNameResolver(IHttpContextAccessor httpContextAccessor)
        {
            this.httpContextAccessor = httpContextAccessor;
        }

        /// <inheritdoc />
#pragma warning disable CA1055 // Uri return values should not be strings
        public string GetBaseUrl()
#pragma warning restore CA1055 // Uri return values should not be strings
        {
            return $"{this.httpContextAccessor.HttpContext.Request.Scheme}://{this.GetHost().Value}";
        }

        private HostString GetHost()
        {
            var request = this.httpContextAccessor.HttpContext.Request;
            var requestHost = request.Host;

            if (request.Headers.TryGetValue("X-Forwarded-Host", out var host))
            {
                var hostString = host.FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(hostString))
                {
                    requestHost = new HostString(hostString);
                }
            }

            return requestHost;
        }
    }
}
