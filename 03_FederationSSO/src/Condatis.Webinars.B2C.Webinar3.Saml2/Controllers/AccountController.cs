﻿// <copyright file="AccountController.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar3.Saml2.Controllers
{
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authentication;
    using Microsoft.AspNetCore.Authentication.Cookies;
    using Microsoft.AspNetCore.Mvc;
    using Sustainsys.Saml2.AspNetCore2;

    /// <summary>
    /// Account.
    /// </summary>
    public class AccountController : Controller
    {
        /// <summary>
        /// Login.
        /// </summary>
        /// <param name="returnUrl">URL to redirect to after logout.</param>
        /// <returns>An <see cref="IActionResult"/>.</returns>
#pragma warning disable CA1054 // Uri parameters should not be strings
        public IActionResult Login(string returnUrl)
#pragma warning restore CA1054 // Uri parameters should not be strings
        {
            this.ViewData["ReturnUrl"] = returnUrl;

            return this.Challenge(new AuthenticationProperties { RedirectUri = this.Url.Action("Index", "Home") }, Saml2Defaults.Scheme);
        }

        /// <summary>
        /// Logout local.
        /// </summary>
        /// <returns>An <see cref="IActionResult"/>.</returns>
        public async Task<IActionResult> LogoutLocal()
        {
            await this.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme).ConfigureAwait(true);
            return this.RedirectToAction("LoggedOutLocal");
        }

        /// <summary>
        /// Logout.
        /// </summary>
        /// <returns>An <see cref="IActionResult"/>.</returns>
        public async Task<IActionResult> Logout()
        {
            await this.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme).ConfigureAwait(true);
            return this.SignOut(new AuthenticationProperties { RedirectUri = this.Url.Action("LoggedOut", "Account") }, Saml2Defaults.Scheme);
        }

        /// <summary>
        /// End session.
        /// </summary>
        /// <returns>An <see cref="IActionResult"/>.</returns>
        public async Task<IActionResult> EndSession()
        {
            await this.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return this.Ok();
        }

        /// <summary>
        /// Logged out.
        /// </summary>
        /// <returns>An <see cref="IActionResult"/>.</returns>
        public IActionResult LoggedOut()
        {
            return this.View();
        }

        /// <summary>
        /// Logged out.
        /// </summary>
        /// <returns>An <see cref="IActionResult"/>.</returns>
        public IActionResult LoggedOutLocal()
        {
            return this.View();
        }
    }
}