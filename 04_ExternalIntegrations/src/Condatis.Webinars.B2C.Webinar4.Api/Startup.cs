// <copyright file="Startup.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api
{
    using Condatis.Webinars.B2C.Webinar4.Api.Routing;
    using Condatis.Webinars.B2C.Webinar4.Api.Services;
    using Condatis.Webinars.B2C.Webinar4.Api.Services.Http;
    using Condatis.Webinars.B2C.Webinar4.Api.Services.Identity;
    using Condatis.Webinars.B2C.Webinar4.Api.Services.Passwords;
    using Condatis.Webinars.B2C.Webinar4.Api.Services.Storage;
    using Flurl.Http;
    using Flurl.Http.Configuration;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc.ApplicationModels;
    using Microsoft.AspNetCore.Rewrite;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.DependencyInjection.Extensions;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Identity.Web;

    /// <summary>
    /// Startup.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="configuration">Configuration.</param>
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        /// <summary>
        /// Configure services.
        /// </summary>
        /// <param name="services">Services.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddApplicationInsightsTelemetry();

            services.AddTransient<IVersionNumberAccessor, VersionNumberAccessor>();
            services.AddTransient<IModelStateConverter, ModelStateConverter>();

            services.AddSingleton<IFlurlClientFactory, PerBaseUrlFlurlClientFactory>();
            FlurlHttp.Configure(opt =>
            {
                opt.HttpClientFactory = new PolicyHttpClientFactory();
            });

            services.AddScoped<IPasswordValidator, ZxcvbnPasswordValidator>();
            services.AddScoped<IPasswordValidator, PwnedPasswordValidator>();

            services.AddAuthorizationPolicies();
            services.AddMicrosoftIdentityWebApiAuthentication(this.Configuration, "Authorization:AzureAd");

            services.AddDbContext<LegacyIdentityContext>(opt =>
            {
                opt.UseSqlServer(this.Configuration.GetConnectionString("Sql"));
            });

            services.AddIdentityCore<LegacyUser>()
                    .AddEntityFrameworkStores<LegacyIdentityContext>();

            services.AddHealthChecks();

            services.AddControllers(opt =>
            {
                opt.Conventions.Add(new RouteTokenTransformerConvention(new SlugifyParameterTransformer()));
            })
            .AddJsonOptions(opt =>
            {
                opt.JsonSerializerOptions.IgnoreNullValues = true;
            });

            services.AddRouting(opt =>
            {
                opt.LowercaseUrls = true;
                opt.LowercaseQueryStrings = false;
                opt.ConstraintMap["slugify"] = typeof(SlugifyParameterTransformer);
            });
        }

        /// <summary>
        /// Configure.
        /// </summary>
        /// <param name="app">App.</param>
        /// <param name="env">Env.</param>
        /// <param name="db">Legacy user db.</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, LegacyIdentityContext db)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseCsp(opts =>
                {
                    opts.UpgradeInsecureRequests();
                });
            }
            else
            {
                app.UseHsts();

                app.UseCsp(opts =>
                {
                    opts.UpgradeInsecureRequests();
                });
            }

            db.Database.Migrate();

            app.UseXContentTypeOptions();
            app.UseReferrerPolicy(opts => opts.NoReferrer());

            app.UseHttpsRedirection();

            app.UseXXssProtection(opt => opt.EnabledWithBlockMode());
            app.UseNoCacheHttpHeaders();
            app.UseXfo(xfo => xfo.Deny());
            app.UseRedirectValidation(opts =>
            {
                opts.AllowSameHostRedirectsToHttps();
            });

            var rewriteOptions = new RewriteOptions();
            rewriteOptions.Rules.Add(new RedirectLowercaseRule());
            app.UseRewriter(rewriteOptions);

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHealthChecks("/health");
            });
        }
    }
}
