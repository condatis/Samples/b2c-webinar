﻿/// <binding BeforeBuild='default' ProjectOpened='watch' />
"use strict";

const gulp = require("gulp");
const cleanCSS = require("gulp-clean-css");
const uglify = require("gulp-uglify");
const rename = require("gulp-rename");

const paths = {
    webroot: "./wwwroot/"
};

paths.css = `${paths.webroot}css/**/*.css`;
paths.minCss = `${paths.webroot}css/**/*.min.css`;
paths.libCss = `${paths.webroot}lib/**/*.min.css`;
paths.siteCssName = "site.min.css";
paths.siteCss = `${paths.webroot}css`;
paths.js = `${paths.webroot}js/**/*.js`;
paths.minJs = `${paths.webroot}js/**/*.min.js`;

gulp.task("min:css", (done) => {
    gulp.src([paths.css, `!${paths.minCss}`])
        .pipe(cleanCSS())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(function(f) {
            return f.base;
        }));

    done();
});

gulp.task("min:js", (done) => {
    gulp.src([paths.js, `!${paths.minJs}`])
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(function(f) {
            return f.base;
        }));

    done();
});

var defaultTask = (cb) => {
    return gulp.series("min:css", "min:js")(cb); 
};

gulp.task("default", defaultTask);

gulp.task('watch', function() {
    gulp.watch([
        paths.css,
        `!${paths.minCss}`,
        paths.libCss
    ], defaultTask);
});