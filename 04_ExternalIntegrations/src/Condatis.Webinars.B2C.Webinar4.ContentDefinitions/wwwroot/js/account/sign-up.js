﻿// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

(function($) {

    function delay(callback, ms) {
        var timer = 0;
        return function() {
            var context = this;
            var args = arguments;

            clearTimeout(timer);

            timer = setTimeout(function () {
                callback.apply(context, args);
            }, ms || 0);
        };
    }

    $(function() {
        var $password = $('#password');

        $password.closest('.attrEntry').before($('<hr class="my-5">'));
        $('#reenterPassword').closest('.attrEntry').after($('<hr class="my-5">'));
        $('#continue').closest('.btn-group').addClass('mt-5');

        $password.on('keyup', delay(function(e) {

            var $error = $password.closest('.attrEntry').find('.error');

            $.ajax({
                url: 'https://condatisb2cwebinar-api.azurewebsites.net/passwords/validate',
                method: 'POST',
                data: JSON.stringify({ password: $password.val() }),
                dataType: 'json',
                contentType: 'application/json'
            })
            .done(function(response) {

                if (response.isValid) {
                    if ($error.text() == '') {
                        $error.toggle(false);
                    }
                }
                else {

                    var currentError = $error.text();
                    if (currentError.length > 0) {
                        response.validationErrors.push(currentError);
                    }

                    $error.html(response.validationErrors.join('<br>'));
                    $error.toggle(true);
                }
            });
        }, parseInt(SETTINGS.config.inputVerificationDelayTimeInMilliseconds) + 100));
    });

})(jQuery);