﻿// <copyright file="IVersionNumberAccessor.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Services
{
    /// <summary>
    /// Provides a way to access the current version number the API.
    /// </summary>
    public interface IVersionNumberAccessor
    {
        /// <summary>
        /// Gets the current API version number.
        /// </summary>
        /// <returns>The API version number.</returns>
        string GetVersionNumber();
    }
}