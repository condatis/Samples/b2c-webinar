﻿// <copyright file="LegacyUsersController.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Controllers
{
    using System;
    using System.Threading.Tasks;
    using Condatis.Webinars.B2C.Webinar4.Api.Models.LegacyUsers;
    using Condatis.Webinars.B2C.Webinar4.Api.Services;
    using Condatis.Webinars.B2C.Webinar4.Api.Services.Identity;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Legacy users.
    /// </summary>
    [ApiController]
    public class LegacyUsersController : B2CController
    {
        private readonly UserManager<LegacyUser> userManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="LegacyUsersController"/> class.
        /// </summary>
        /// <param name="modelStateConverter">Model state converter.</param>
        /// <param name="userManager">User manager.</param>
        public LegacyUsersController(IModelStateConverter modelStateConverter, UserManager<LegacyUser> userManager)
            : base(modelStateConverter)
        {
            this.userManager = userManager;
        }

        /// <summary>
        /// Get legacy user.
        /// </summary>
        /// <param name="request">Request.</param>
        /// <returns>Response.</returns>
        [HttpPost]
        [Authorize(Policy = PolicyNames.LegacyUsers)]
        [Route(RouteTypes.B2C.ControllerAction)]
        public async Task<ActionResult<LegacyUserResponse>> Authenticate(AuthenticateLegacyUserRequest request)
        {
            try
            {
                if (!this.ModelState.IsValid)
                {
                    return this.BadRequest(this.ModelState);
                }

                var legacyUser = await this.userManager.FindByEmailAsync(request.EmailAddress);
                if (legacyUser is null)
                {
                    this.ModelState.AddModelError(string.Empty, $"User '{request.EmailAddress}' was not found in legacy system.");
                    return this.BadRequest(this.ModelState);
                }

                var verificationResult = this.userManager.PasswordHasher.VerifyHashedPassword(legacyUser, legacyUser.PasswordHash, request.Password);
                if (verificationResult == PasswordVerificationResult.Failed)
                {
                    this.ModelState.AddModelError(string.Empty, $"Invalid password for user '{request.EmailAddress}' in legacy system.");
                    return this.BadRequest(this.ModelState);
                }

                return this.Ok(new LegacyUserResponse(legacyUser));
            }
            catch (Exception ex)
            {
                throw new B2CException(ex.Message, "Failed authenticating against legacy system.", ex);
            }
        }

        /// <summary>
        /// Create legacy user.
        /// </summary>
        /// <param name="request">Request.</param>
        /// <returns>Response.</returns>
        [HttpPost]
        [Authorize(Policy = PolicyNames.LegacyUsers)]
        [Route(RouteTypes.Default.Controller)]
        public async Task<ActionResult<LegacyUserResponse>> Post(CreateLegacyUserRequest request)
        {
            if (!this.ModelState.IsValid)
            {
                return this.BadRequest(this.ModelState);
            }

            var legacyUser = new LegacyUser
            {
                UserName = request.EmailAddress,
                Email = request.EmailAddress,
                EmailConfirmed = true,
                Forename = request.Forename,
                Surname = request.Surname,
            };

            var result = await this.userManager.CreateAsync(legacyUser, request.Password);
            if (!result.Succeeded)
            {
                foreach (var error in result.Errors)
                {
                    this.ModelState.AddModelError(string.Empty, error.Description);
                }

                return this.BadRequest(this.ModelState);
            }

            return this.CreatedAtAction("Get", new { id = legacyUser.Id }, new LegacyUserResponse(legacyUser));
        }

        /// <summary>
        /// Get legacy user.
        /// </summary>
        /// <param name="id">User ID.</param>
        /// <returns>Response.</returns>
        [Authorize(Policy = PolicyNames.LegacyUsers)]
        [Route(RouteTypes.Default.ControllerId)]
        public async Task<ActionResult<LegacyUserResponse>> Get(string id)
        {
            var legacyUser = await this.userManager.FindByIdAsync(id);
            if (legacyUser is null)
            {
                throw new NotFoundException($"User '{id}' was not found.");
            }

            return this.Ok(new LegacyUserResponse(legacyUser));
        }
    }
}
