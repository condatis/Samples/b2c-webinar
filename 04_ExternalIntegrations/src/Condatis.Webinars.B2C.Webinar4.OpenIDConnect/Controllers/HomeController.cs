﻿// <copyright file="HomeController.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.OpenIDConnect.Controllers
{
    using System.Diagnostics;
    using Condatis.Webinars.B2C.Webinar4.OpenIDConnect.Models;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Home.
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// Index.
        /// </summary>
        /// <returns>An <see cref="IActionResult"/>.</returns>
        public IActionResult Index()
        {
            return this.View();
        }

        /// <summary>
        /// Error.
        /// </summary>
        /// <param name="message">Message to show to the user.</param>
        /// <returns>An <see cref="IActionResult"/>.</returns>
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error(string message)
        {
            return this.View(new ErrorViewModel
            {
                RequestId = Activity.Current?.Id ?? this.HttpContext.TraceIdentifier,
                Message = message,
            });
        }
    }
}
