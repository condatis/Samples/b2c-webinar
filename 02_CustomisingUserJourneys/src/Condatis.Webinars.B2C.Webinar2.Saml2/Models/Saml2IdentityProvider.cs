﻿// <copyright file="Saml2IdentityProvider.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar2.Saml2.Models
{
    /// <summary>
    /// Provides configuration information about a SAML IdP.
    /// </summary>
    public class Saml2IdentityProvider
    {
        /// <summary>
        /// Gets or sets the entity Id.
        /// </summary>
        public string EntityId { get; set; }

        /// <summary>
        /// Gets or sets the metadata location.
        /// </summary>
        public string MetadataLocation { get; set; }

        /// <summary>
        /// Gets or sets whether to allow unsolicited authentication responses.
        /// </summary>
        public bool AllowUnsolicitedAuthnResponse { get; set; }
    }
}