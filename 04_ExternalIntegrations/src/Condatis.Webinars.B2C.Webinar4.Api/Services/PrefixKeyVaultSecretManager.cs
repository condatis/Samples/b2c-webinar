﻿// <copyright file="PrefixKeyVaultSecretManager.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Services
{
    using System;
    using Microsoft.Azure.KeyVault.Models;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Configuration.AzureKeyVault;

    /// <summary>
    /// Provides a way of accessing Key Vault secrets prefixed with a given value.
    /// </summary>
    public class PrefixKeyVaultSecretManager : IKeyVaultSecretManager
    {
        private const string KeyVaultKeyDelimiter = "--";
        private readonly string prefix;

        /// <summary>
        /// Initializes a new instance of the <see cref="PrefixKeyVaultSecretManager"/> class.
        /// </summary>
        /// <param name="prefix">Prefix.</param>
        public PrefixKeyVaultSecretManager(string prefix)
        {
            this.prefix = prefix + KeyVaultKeyDelimiter;
        }

        /// <inheritdoc />
        public bool Load(SecretItem secret)
        {
            return secret?.Identifier.Name
                         .StartsWith(this.prefix, StringComparison.InvariantCultureIgnoreCase) == true;
        }

        /// <inheritdoc />
        public string GetKey(SecretBundle secret)
        {
            return secret?.SecretIdentifier.Name
                         .Substring(this.prefix.Length)
                         .Replace(KeyVaultKeyDelimiter, ConfigurationPath.KeyDelimiter, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
