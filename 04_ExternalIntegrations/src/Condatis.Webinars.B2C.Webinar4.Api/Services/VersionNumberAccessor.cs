﻿// <copyright file="VersionNumberAccessor.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Services
{
    using System;

    /// <inheritdoc />
    public class VersionNumberAccessor : IVersionNumberAccessor
    {
        private readonly Lazy<string> versionNumber = new Lazy<string>(LoadVersionNumber);

        /// <inheritdoc />
        public string GetVersionNumber()
        {
            return this.versionNumber.Value;
        }

        private static string LoadVersionNumber()
        {
            return typeof(Program).Assembly.GetName().Version.ToString();
        }
    }
}
