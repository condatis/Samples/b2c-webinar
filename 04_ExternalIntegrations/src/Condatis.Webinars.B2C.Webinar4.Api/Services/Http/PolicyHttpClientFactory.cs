﻿// <copyright file="PolicyHttpClientFactory.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Services.Http
{
    using System.Net.Http;
    using Flurl.Http.Configuration;

    /// <summary>
    /// Factory for creating HTTP clients that use a <see cref="HttpPolicyHandler"/> when executing requests.
    /// </summary>
    public class PolicyHttpClientFactory : DefaultHttpClientFactory
    {
        /// <inheritdoc />
        public override HttpMessageHandler CreateMessageHandler()
        {
            return new HttpPolicyHandler
            {
                InnerHandler = base.CreateMessageHandler(),
            };
        }
    }
}
