﻿// <copyright file="AbsoluteUrlHelperFactory.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar3.ContentDefinitions.Services.Web
{
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;

    /// <inheritdoc />
    public class AbsoluteUrlHelperFactory : IUrlHelperFactory
    {
        private readonly IUrlHelperFactory urlHelperFactory;
        private readonly IHostNameResolver hostNameResolver;

        /// <summary>
        /// Initializes a new instance of the <see cref="AbsoluteUrlHelperFactory"/> class.
        /// </summary>
        /// <param name="urlHelperFactory">URL helper factory.</param>
        /// <param name="hostNameResolver">Hostname resolver.</param>
        public AbsoluteUrlHelperFactory(IUrlHelperFactory urlHelperFactory, IHostNameResolver hostNameResolver)
        {
            this.urlHelperFactory = urlHelperFactory;
            this.hostNameResolver = hostNameResolver;
        }

        /// <inheritdoc />
        public IUrlHelper GetUrlHelper(ActionContext context)
        {
            return new AbsoluteUrlHelper(this.urlHelperFactory.GetUrlHelper(context), this.hostNameResolver);
        }
    }
}
