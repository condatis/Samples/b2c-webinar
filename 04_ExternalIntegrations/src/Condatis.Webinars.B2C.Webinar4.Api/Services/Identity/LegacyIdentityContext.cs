﻿// <copyright file="LegacyIdentityContext.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Services.Identity
{
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// EntityFrameworkCore context for Identity access.
    /// </summary>
    public class LegacyIdentityContext : IdentityDbContext<LegacyUser>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LegacyIdentityContext"/> class.
        /// </summary>
        /// <param name="options">Options.</param>
        public LegacyIdentityContext(DbContextOptions<LegacyIdentityContext> options)
            : base(options)
        {
        }

        /// <inheritdoc />
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
    }
}
