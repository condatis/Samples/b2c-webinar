<!-- 
  Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
  Licensed under the MIT license. See LICENSE file in the project root for full license information.
-->

<TrustFrameworkPolicy
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema"
  xmlns="http://schemas.microsoft.com/online/cpim/schemas/2013/06"
  PolicySchemaVersion="0.3.0.0"
  TenantId="#{Policy.B2C.TenantDomain}#"
  PolicyId="#{Policy.PolicyId.SignIn.Base}#"
  PublicPolicyUri="#{Policy.PublicPolicyUri}#/#{Policy.PolicyId.SignIn.Base}#"
  DeploymentMode="#{Policy.DeploymentMode}#"
  UserJourneyRecorderEndpoint="urn:journeyrecorder:applicationinsights"
  >
  <BasePolicy>
    <TenantId>#{Policy.B2C.TenantDomain}#</TenantId>
    <PolicyId>#{Policy.PolicyId.Extensions}#</PolicyId>
  </BasePolicy>

  <BuildingBlocks>
    <ClaimsSchema>
      <ClaimType Id="newUser">
        <DataType>boolean</DataType>
      </ClaimType>
    </ClaimsSchema>
    <ClaimsTransformations>
      <ClaimsTransformation Id="CreateRegistrationPassword" TransformationMethod="FormatStringClaim">
        <InputClaims>
          <InputClaim ClaimTypeReferenceId="signInPassword" TransformationClaimType="inputClaim" />
        </InputClaims>
        <InputParameters>
          <InputParameter Id="stringFormat" DataType="string" Value="{0}" />
        </InputParameters>
        <OutputClaims>
          <OutputClaim ClaimTypeReferenceId="password" TransformationClaimType="outputClaim" />
        </OutputClaims>
      </ClaimsTransformation>
    </ClaimsTransformations>

  </BuildingBlocks>

  <ClaimsProviders>

    <!-- User Login -->

    <ClaimsProvider>
      <DisplayName>User Signin</DisplayName>
      <TechnicalProfiles>
        <TechnicalProfile Id="UserAccount-SignIn">
          <DisplayName>User Registration/Login</DisplayName>
          <Description>Register or log in to your account</Description>
          <Protocol Name="Proprietary" Handler="Web.TPEngine.Providers.SelfAssertedAttributeProvider, Web.TPEngine, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" />
          <Metadata>
            <Item Key="ContentDefinitionReferenceId">content.signin</Item>
            <Item Key="SignUpTarget">UserRegistration</Item>
            <Item Key="setting.operatingMode">Email</Item>
            <Item Key="setting.showCancelButton">false</Item>
            <Item Key="setting.forgotPasswordLinkLocation">None</Item>
          </Metadata>
          <IncludeInSso>false</IncludeInSso>
          <OutputClaims>
            <OutputClaim ClaimTypeReferenceId="signInEmail" />
            <OutputClaim ClaimTypeReferenceId="signInPassword" Required="true" />

            <OutputClaim ClaimTypeReferenceId="objectId" />
            <OutputClaim ClaimTypeReferenceId="givenName"/>
            <OutputClaim ClaimTypeReferenceId="surname"/>
            <OutputClaim ClaimTypeReferenceId="displayName" />
            <OutputClaim ClaimTypeReferenceId="authenticationSource" />
          </OutputClaims>
          <ValidationTechnicalProfiles>
            <ValidationTechnicalProfile ReferenceId="UserAccount-SignIn-NonInteractive-Email" ContinueOnSuccess="false" ContinueOnError="true" />
            <ValidationTechnicalProfile ReferenceId="API-AuthenticateLegacyUser">
              <Preconditions>
                <Precondition Type="ClaimsExist" ExecuteActionsIf="true">
                  <Value>objectId</Value>
                  <Action>SkipThisValidationTechnicalProfile</Action>
                </Precondition>
              </Preconditions>
            </ValidationTechnicalProfile>
            <ValidationTechnicalProfile ReferenceId="AAD-CreateUser-Email">
              <Preconditions>
                <Precondition Type="ClaimsExist" ExecuteActionsIf="true">
                  <Value>objectId</Value>
                  <Action>SkipThisValidationTechnicalProfile</Action>
                </Precondition>
              </Preconditions>
            </ValidationTechnicalProfile>
          </ValidationTechnicalProfiles>
          <UseTechnicalProfileForSessionManagement ReferenceId="SM-AAD"/>
        </TechnicalProfile>

        <TechnicalProfile Id="UserAccount-SignIn-NonInteractive-Email">
          <DisplayName>User Login</DisplayName>
          <Protocol Name="OpenIdConnect" />
          <Metadata>
            <Item Key="UserMessageIfClaimsPrincipalDoesNotExist">Please check your sign in details</Item>
            <Item Key="UserMessageIfInvalidPassword">Please check your sign in details</Item>
            <Item Key="UserMessageIfOldPasswordUsed">Please check your sign in details</Item>

            <Item Key="ProviderName">https://sts.windows.net/</Item>
            <Item Key="METADATA">#{Policy.B2C.OpenIdConfiguration}#</Item>
            <Item Key="authorization_endpoint">#{Policy.B2C.AuthorizationEndpoint}#</Item>
            <Item Key="client_id">#{Policy.B2C.PolicyEngineProxy.ClientId}#</Item>
            <Item Key="IdTokenAudience">#{Policy.B2C.PolicyEngine.ClientId}#</Item>
            <Item Key="response_types">id_token</Item>
            <Item Key="response_mode">query</Item>
            <Item Key="scope">email openid</Item>

            <Item Key="UsePolicyInRedirectUri">false</Item>
            <Item Key="HttpBinding">POST</Item>
          </Metadata>
          <InputClaims>
            <InputClaim ClaimTypeReferenceId="client_id" DefaultValue="#{Policy.B2C.PolicyEngineProxy.ClientId}#" />
            <InputClaim ClaimTypeReferenceId="resource_id" PartnerClaimType="resource" DefaultValue="#{Policy.B2C.PolicyEngine.ClientId}#" />
            <InputClaim ClaimTypeReferenceId="signInEmail" PartnerClaimType="username" Required="true" />
            <InputClaim ClaimTypeReferenceId="signInPassword" PartnerClaimType="password" Required="true" />
            <InputClaim ClaimTypeReferenceId="grant_type" DefaultValue="password" AlwaysUseDefaultValue="true" />
            <InputClaim ClaimTypeReferenceId="scope" DefaultValue="openid" AlwaysUseDefaultValue="true" />
            <InputClaim ClaimTypeReferenceId="nca" PartnerClaimType="nca" DefaultValue="1" AlwaysUseDefaultValue="true" />
          </InputClaims>
          <OutputClaims>
            <OutputClaim ClaimTypeReferenceId="objectId" PartnerClaimType="oid" />
            <OutputClaim ClaimTypeReferenceId="givenName" PartnerClaimType="given_name" />
            <OutputClaim ClaimTypeReferenceId="surname" PartnerClaimType="family_name" />
            <OutputClaim ClaimTypeReferenceId="userPrincipalName" PartnerClaimType="upn" />
            <OutputClaim ClaimTypeReferenceId="authenticationSource" DefaultValue="localAccount" AlwaysUseDefaultValue="true" />
          </OutputClaims>
          <OutputClaimsTransformations>
            <OutputClaimsTransformation ReferenceId="CreateDisplayName"/>
          </OutputClaimsTransformations>
        </TechnicalProfile>

        <TechnicalProfile Id="API-AuthenticateLegacyUser">
          <DisplayName>Authenticate legacy user</DisplayName>
          <Protocol Name="Proprietary" Handler="Web.TPEngine.Providers.RestfulProvider, Web.TPEngine, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" />
          <Metadata>
            <Item Key="ServiceUrl">#{Policy.Url.Api}#/legacy-users/authenticate</Item>
            <Item Key="AuthenticationType">Bearer</Item>
            <Item Key="UseClaimAsBearerToken">bearerToken</Item>
            <Item Key="SendClaimsIn">Body</Item>
            <Item Key="AllowInsecureAuthInProduction">false</Item>
            <Item Key="DebugMode">#{Policy.DebugMode}#</Item>
          </Metadata>
          <InputClaims>
            <InputClaim ClaimTypeReferenceId="bearerToken"/>
            <InputClaim ClaimTypeReferenceId="signInEmail" PartnerClaimType="emailAddress" />
            <InputClaim ClaimTypeReferenceId="signInPassword" PartnerClaimType="password" />
          </InputClaims>
          <OutputClaims>
            <OutputClaim ClaimTypeReferenceId="givenName" PartnerClaimType="forename" />
            <OutputClaim ClaimTypeReferenceId="surname" PartnerClaimType="surname" />
            <OutputClaim ClaimTypeReferenceId="authenticationSource" DefaultValue="migratedAccount" AlwaysUseDefaultValue="true" />
          </OutputClaims>
          <OutputClaimsTransformations>
            <OutputClaimsTransformation ReferenceId="CreateDisplayName" />
            <OutputClaimsTransformation ReferenceId="CreateRegistrationPassword" />
          </OutputClaimsTransformations>
        </TechnicalProfile>

        <!-- Logging -->
        <TechnicalProfile Id="AppInsights-SignInSuccess">
          <InputClaims>
            <InputClaim ClaimTypeReferenceId="EventType" PartnerClaimType="eventName" DefaultValue="SignInSuccess" />
          </InputClaims>
          <IncludeTechnicalProfile ReferenceId="AppInsights-Common" />
        </TechnicalProfile>
      </TechnicalProfiles>
    </ClaimsProvider>

    <!-- User Registration -->

    <ClaimsProvider>
      <DisplayName>User Signup</DisplayName>
      <TechnicalProfiles>
        <TechnicalProfile Id="UserAccount-SignUp">
          <DisplayName>User Registration</DisplayName>
          <Description>Register for your account</Description>
          <Protocol Name="Proprietary" Handler="Web.TPEngine.Providers.SelfAssertedAttributeProvider, Web.TPEngine, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" />
          <Metadata>
            <Item Key="ContentDefinitionReferenceId">content.signup</Item>
            <Item Key="setting.showCancelButton">false</Item>
            <Item Key="setting.inputVerificationDelayTimeInMilliseconds">500</Item>
          </Metadata>
          <IncludeInSso>false</IncludeInSso>
          <OutputClaims>
            <OutputClaim ClaimTypeReferenceId="signInEmail" PartnerClaimType="Verified.Email" Required="true" />

            <OutputClaim ClaimTypeReferenceId="password" Required="true" />
            <OutputClaim ClaimTypeReferenceId="reenterPassword" Required="true" />
            
            <OutputClaim ClaimTypeReferenceId="givenName" Required="true" />
            <OutputClaim ClaimTypeReferenceId="surname" Required="true" />
            <OutputClaim ClaimTypeReferenceId="extension_referrer" />

            <OutputClaim ClaimTypeReferenceId="objectId" />
            <OutputClaim ClaimTypeReferenceId="displayName" />

            <OutputClaim ClaimTypeReferenceId="authenticationSource" DefaultValue="localAccount" AlwaysUseDefaultValue="true" />
            <OutputClaim ClaimTypeReferenceId="newUser" DefaultValue="true" AlwaysUseDefaultValue="true" />
          </OutputClaims>
          <ValidationTechnicalProfiles>
            <ValidationTechnicalProfile ReferenceId="API-ValidatePassword" />
            <ValidationTechnicalProfile ReferenceId="AAD-CreateUser-Email" />
          </ValidationTechnicalProfiles>
          <UseTechnicalProfileForSessionManagement ReferenceId="SM-AAD"/>
        </TechnicalProfile>

        <TechnicalProfile Id="AAD-CreateUser-Email">
          <DisplayName>Create user by email</DisplayName>
          <Metadata>
            <Item Key="Operation">Write</Item>
            <Item Key="RaiseErrorIfClaimsPrincipalAlreadyExists">true</Item>
            <Item Key="UserMessageIfClaimsPrincipalAlreadyExists">You are already registered, please press the back button and sign in instead.</Item>
          </Metadata>
          <InputClaimsTransformations>
            <InputClaimsTransformation ReferenceId="CreateDisplayName" />
          </InputClaimsTransformations>
          <InputClaims>
            <InputClaim ClaimTypeReferenceId="signInEmail" PartnerClaimType="signInNames.emailAddress"/>
          </InputClaims>
          <PersistedClaims>
            <PersistedClaim ClaimTypeReferenceId="signInEmail" PartnerClaimType="signInNames.emailAddress"/>
            <PersistedClaim ClaimTypeReferenceId="password"/>
            <PersistedClaim ClaimTypeReferenceId="passwordPolicies" DefaultValue="DisablePasswordExpiration,DisableStrongPassword"/>
            <PersistedClaim ClaimTypeReferenceId="givenName"/>
            <PersistedClaim ClaimTypeReferenceId="surname"/>
            <PersistedClaim ClaimTypeReferenceId="displayName" PartnerClaimType="displayName"/>
            <PersistedClaim ClaimTypeReferenceId="extension_referrer" />
          </PersistedClaims>
          <OutputClaims>
            <OutputClaim ClaimTypeReferenceId="objectId" />
            <OutputClaim ClaimTypeReferenceId="userPrincipalName" />
            <OutputClaim ClaimTypeReferenceId="displayName" />
          </OutputClaims>
          <IncludeTechnicalProfile ReferenceId="AAD-Common"/>
        </TechnicalProfile>

        <!-- Logging -->
        <TechnicalProfile Id="AppInsights-RegistrationSuccess">
          <InputClaims>
            <InputClaim ClaimTypeReferenceId="EventType" PartnerClaimType="eventName" DefaultValue="RegistrationSuccess" />
          </InputClaims>
          <IncludeTechnicalProfile ReferenceId="AppInsights-Common" />
        </TechnicalProfile>
      </TechnicalProfiles>
    </ClaimsProvider>

  </ClaimsProviders>

  <SubJourneys>
    <SubJourney Id="LocalSignIn" Type="Call">
      <OrchestrationSteps>
          
          <!-- Show combined sign in/sign up page -->
          <OrchestrationStep Order="1" Type="CombinedSignInAndSignUp" ContentDefinitionReferenceId="content.signin">   
            <ClaimsProviderSelections>
              <ClaimsProviderSelection ValidationClaimsExchangeId="UserLogin"/>
            </ClaimsProviderSelections>
            <ClaimsExchanges>
              <ClaimsExchange Id="UserLogin" TechnicalProfileReferenceId="UserAccount-SignIn"/>
            </ClaimsExchanges>
          </OrchestrationStep>

          <!-- Show sign-up page -->
          <OrchestrationStep Order="2" Type="ClaimsExchange">
            <Preconditions>
              <Precondition Type="ClaimsExist" ExecuteActionsIf="true">
                <Value>objectId</Value>
                <Action>SkipThisOrchestrationStep</Action>
              </Precondition>
            </Preconditions>
            <ClaimsExchanges>
              <ClaimsExchange Id="UserRegistration" TechnicalProfileReferenceId="UserAccount-SignUp"/>
            </ClaimsExchanges>
          </OrchestrationStep>

          <!-- Read local user -->
          <OrchestrationStep Order="3" Type="ClaimsExchange">
            <ClaimsExchanges>
              <ClaimsExchange Id="ReadLocalUser" TechnicalProfileReferenceId="AAD-ReadUser-ById"/>
            </ClaimsExchanges>
          </OrchestrationStep>
          
        </OrchestrationSteps>
    </SubJourney>
  </SubJourneys>

</TrustFrameworkPolicy>