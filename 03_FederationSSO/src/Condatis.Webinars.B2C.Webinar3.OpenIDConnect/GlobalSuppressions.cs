﻿// <copyright file="GlobalSuppressions.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1054:URI-like parameters should not be strings", Justification = "They often should.")]
