﻿// <copyright file="PolicyNames.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar3.OpenIDConnect
{
    /// <summary>
    /// Names of authentication policies.
    /// </summary>
    public static class PolicyNames
    {
        /// <summary>
        /// Policy which requires LoA 1.
        /// </summary>
        public const string LoA1 = nameof(LoA1);

        /// <summary>
        /// Policy which requires LoA 2.
        /// </summary>
        public const string LoA2 = nameof(LoA2);
    }
}