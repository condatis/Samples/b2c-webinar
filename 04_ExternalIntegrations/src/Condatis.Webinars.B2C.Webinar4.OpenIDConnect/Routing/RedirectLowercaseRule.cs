﻿// <copyright file="RedirectLowercaseRule.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.OpenIDConnect.Routing
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Net;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Rewrite;
    using Microsoft.Net.Http.Headers;

    /// <summary>
    /// Provides a routing rule that redirects to the lower-case version of a URL.
    /// </summary>
    public class RedirectLowercaseRule : IRule
    {
        /// <summary>
        /// Applies the rule.
        /// </summary>
        /// <param name="context">Context.</param>
        public void ApplyRule(RewriteContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            var request = context.HttpContext.Request;

            if (!HasUpperChar(request.Path) && !HasUpperChar(request.Host))
            {
                context.Result = RuleResult.ContinueRules;
                return;
            }

            var response = context.HttpContext.Response;
            response.StatusCode = (int)HttpStatusCode.MovedPermanently;
#pragma warning disable CA1308 // Normalize strings to uppercase
            response.Headers[HeaderNames.Location] = $"{request.Scheme}://{request.Host.Value}{request.PathBase}{request.Path}".ToLower(CultureInfo.InvariantCulture) + request.QueryString;
#pragma warning restore CA1308 // Normalize strings to uppercase
            context.Result = RuleResult.EndResponse;
        }

        private static bool HasUpperChar(HostString host)
        {
            return host.HasValue && host.Value.Any(char.IsUpper);
        }

        private static bool HasUpperChar(PathString path)
        {
            return path.HasValue && path.Value.Any(char.IsUpper);
        }
    }
}
