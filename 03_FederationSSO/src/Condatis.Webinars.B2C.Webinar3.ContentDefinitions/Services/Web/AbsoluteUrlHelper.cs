﻿// <copyright file="AbsoluteUrlHelper.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar3.ContentDefinitions.Services.Web
{
    using System;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;

    /// <summary>
    /// Expands relative URLs to absolute URLs.
    /// </summary>
    public class AbsoluteUrlHelper : IUrlHelper
    {
        private readonly IUrlHelper urlHelper;
        private readonly IHostNameResolver hostNameResolver;

        /// <summary>
        /// Initializes a new instance of the <see cref="AbsoluteUrlHelper"/> class.
        /// </summary>
        /// <param name="urlHelper">URL helper.</param>
        /// <param name="hostNameResolver">Hostname resolver.</param>
        public AbsoluteUrlHelper(IUrlHelper urlHelper, IHostNameResolver hostNameResolver)
        {
            this.urlHelper = urlHelper;
            this.hostNameResolver = hostNameResolver;
        }

        /// <inheritdoc />
        public ActionContext ActionContext => this.urlHelper.ActionContext;

        /// <inheritdoc />
        public string Action(UrlActionContext actionContext)
        {
            return this.urlHelper.Action(actionContext);
        }

        /// <inheritdoc />
        public string Content(string contentPath)
        {
            if (this.IsLocalUrl(contentPath))
            {
                return new Uri(new Uri(this.hostNameResolver.GetBaseUrl()), this.urlHelper.Content(contentPath)).ToString();
            }

            return this.urlHelper.Content(contentPath);
        }

        /// <inheritdoc />
        public bool IsLocalUrl(string url)
        {
            return this.urlHelper.IsLocalUrl(url);
        }

        /// <inheritdoc />
        public string Link(string routeName, object values)
        {
            return this.urlHelper.Link(routeName, values);
        }

        /// <inheritdoc />
        public string RouteUrl(UrlRouteContext routeContext)
        {
            return this.urlHelper.RouteUrl(routeContext);
        }
    }
}
