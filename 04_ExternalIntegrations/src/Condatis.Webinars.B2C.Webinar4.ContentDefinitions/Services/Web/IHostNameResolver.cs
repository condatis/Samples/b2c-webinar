﻿// <copyright file="IHostNameResolver.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.ContentDefinitions.Services.Web
{
    /// <summary>
    /// Resolves the Hostname for the request.
    /// </summary>
    public interface IHostNameResolver
    {
        /// <summary>
        /// Gets the base URL {scheme}:\\{hostname}.
        /// </summary>
        /// <returns>A URL.</returns>
#pragma warning disable CA1055 // Uri return values should not be strings
        string GetBaseUrl();
#pragma warning restore CA1055 // Uri return values should not be strings
    }
}