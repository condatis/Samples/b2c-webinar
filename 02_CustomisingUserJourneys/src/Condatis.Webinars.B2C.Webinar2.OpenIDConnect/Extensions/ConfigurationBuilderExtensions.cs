﻿// <copyright file="ConfigurationBuilderExtensions.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Microsoft.Extensions.Configuration.AzureKeyVault
{
    using System;
    using Microsoft.Azure.KeyVault;
    using Microsoft.Azure.Services.AppAuthentication;
    using Microsoft.Extensions.Configuration;

    /// <summary>
    /// ConfigurationBuilder extensions.
    /// </summary>
    public static class ConfigurationBuilderExtensions
    {
        /// <summary>
        /// Adds Azure Key Vault as a configuration provider.
        /// </summary>
        /// <param name="configurationBuilder">Configuration builder.</param>
        /// <param name="keyVaultSecretManager">Key Vault secret manager.</param>
        /// <returns>A <see cref="IConfigurationBuilder"/>.</returns>
        public static IConfigurationBuilder AddAzureKeyVault(this IConfigurationBuilder configurationBuilder, IKeyVaultSecretManager keyVaultSecretManager)
        {
            if (configurationBuilder is null)
            {
                throw new ArgumentNullException(nameof(configurationBuilder));
            }

            var builtConfig = configurationBuilder.Build();

            string keyVaultUrl = builtConfig["KeyVaultUrl"];
            if (!string.IsNullOrWhiteSpace(keyVaultUrl))
            {
                var azureServiceTokenProvider = new AzureServiceTokenProvider();
#pragma warning disable CA2000 // Dispose objects before losing scope
                var keyVaultClient = new KeyVaultClient(
                    new KeyVaultClient.AuthenticationCallback(
                        azureServiceTokenProvider.KeyVaultTokenCallback));
#pragma warning restore CA2000 // Dispose objects before losing scope

                configurationBuilder.AddAzureKeyVault(keyVaultUrl, keyVaultClient, keyVaultSecretManager);
            }

            return configurationBuilder;
        }
    }
}
