﻿// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

(function($) {

    $(function() {
        $('button').addClass('btn').addClass('btn-light');
        $('#verifyCode').removeClass('btn-light').addClass('btn-primary');

        $('#codeVerification').addClass('alert alert-warning');
        $('#codeVerification input[type="text"]').addClass('form-control');
    });

})(jQuery);