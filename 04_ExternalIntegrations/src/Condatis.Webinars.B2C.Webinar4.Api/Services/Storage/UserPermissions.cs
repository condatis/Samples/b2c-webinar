﻿// <copyright file="UserPermissions.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Services.Storage
{
    using Microsoft.Azure.Cosmos.Table;

    /// <summary>
    /// Represents a user.
    /// </summary>
    public class UserPermissions : TableEntity
    {
        /// <summary>
        /// Gets the partition name for users.
        /// </summary>
        public const string PartitionKeyName = "User";

        /// <summary>
        /// Initializes a new instance of the <see cref="UserPermissions"/> class.
        /// </summary>
        public UserPermissions()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserPermissions"/> class.
        /// </summary>
        /// <param name="email">User's email address.</param>
        public UserPermissions(string email)
        {
            this.PartitionKey = PartitionKeyName;
            this.RowKey = email;
        }

        /// <summary>
        /// Gets or sets the groups the user is in.
        /// </summary>
        public string Groups { get; set; }
    }
}
