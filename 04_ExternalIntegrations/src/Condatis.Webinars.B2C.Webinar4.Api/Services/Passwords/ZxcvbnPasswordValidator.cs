﻿// <copyright file="ZxcvbnPasswordValidator.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Services.Passwords
{
    using System.Threading.Tasks;
    using Microsoft.ApplicationInsights;
    using Microsoft.ApplicationInsights.DataContracts;

    /// <summary>
    /// Provides a validator which uses zxcvbn to assess password strength/complexity.
    /// </summary>
    public class ZxcvbnPasswordValidator : IPasswordValidator
    {
        private readonly TelemetryClient telemetryClient;

        /// <summary>
        /// Initializes a new instance of the <see cref="ZxcvbnPasswordValidator"/> class.
        /// </summary>
        /// <param name="telemetryClient">Telemetry client.</param>
        public ZxcvbnPasswordValidator(TelemetryClient telemetryClient)
        {
            this.telemetryClient = telemetryClient;
        }

        /// <inheritdoc />
        public Task<bool> ValidateAsync(string password, IErrorContainer errorContainer)
        {
            var operation = this.telemetryClient.StartOperation<DependencyTelemetry>("zxcvbn password validation");

            try
            {
                var result = Zxcvbn.Core.EvaluatePassword(password);
                if (result.Score > 2)
                {
                    return Task.FromResult(true);
                }

                errorContainer.Add(string.Empty, "Your chosen password can be guessed, please try another.");

                if (!string.IsNullOrWhiteSpace(result.Feedback.Warning))
                {
                    errorContainer.Add(string.Empty, result.Feedback.Warning);
                }

                foreach (var suggestion in result.Feedback.Suggestions)
                {
                    errorContainer.Add(string.Empty, suggestion);
                }

                return Task.FromResult(false);
            }
            finally
            {
                this.telemetryClient.StopOperation(operation);
            }
        }
    }
}