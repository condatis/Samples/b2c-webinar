﻿// <copyright file="IPasswordValidator.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Services.Passwords
{
    using System.Threading.Tasks;

    /// <summary>
    /// Provides a service for checking the number of times a password has been pwned.
    /// </summary>
    public interface IPasswordValidator
    {
        /// <summary>
        /// Finds the number of times a password has been pwned.
        /// </summary>
        /// <param name="password">Password to check.</param>
        /// <param name="errorContainer">Container to which validation errors can be added.</param>
        /// <returns>True if the password can be used.</returns>
        Task<bool> ValidateAsync(string password, IErrorContainer errorContainer);
    }
}