﻿// <copyright file="PostUserRequest.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Models.Users
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Represents the data sent when posting to the /users endpoint.
    /// </summary>
    public class PostUserRequest
    {
        /// <summary>
        /// Gets or sets the unique ID of the user.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the forename of the user.
        /// </summary>
        [MinLength(1)]
        public string Forename { get; set; }

        /// <summary>
        /// Gets or sets the surname of the user.
        /// </summary>
        [MinLength(1)]
        public string Surname { get; set; }
    }
}
