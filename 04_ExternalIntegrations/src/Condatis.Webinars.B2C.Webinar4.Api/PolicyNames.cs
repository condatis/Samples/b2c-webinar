﻿// <copyright file="PolicyNames.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api
{
    /// <summary>
    /// Authorization policy names.
    /// </summary>
    public static class PolicyNames
    {
        /// <summary>
        /// Can read user groups.
        /// </summary>
        public const string ReadUserGroups = "User.Groups.Read";

        /// <summary>
        /// Can write users.
        /// </summary>
        public const string WriteUser = "User.Write";

        /// <summary>
        /// Can access legacy users.
        /// </summary>
        public const string LegacyUsers = "LegacyUsers";

        /// <summary>
        /// Can read users.
        /// </summary>
        public const string Authorised = "Authorised";
    }
}
