// <copyright file="Program.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar2.ContentDefinitions
{
    using System;
    using Condatis.Webinars.B2C.Webinar2.ContentDefinitions.Services.Configuration;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Configuration.AzureKeyVault;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// Program.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// Main.
        /// </summary>
        /// <param name="args">Command line args.</param>
        public static void Main(string[] args)
        {
            // create config needed to log startup errors
            using var env = CreateLoggingHost(args);
            using var serviceScope = env.Services.CreateScope();
            var config = serviceScope.ServiceProvider.GetService<IConfiguration>();

            // create logger for logging startup errors
            using var loggerFactory = Create(config);
            var logger = loggerFactory.CreateLogger<Startup>();

            try
            {
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                logger.LogError(ex, ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Creates an <see cref="IHost"/> that can be used to access the config required to log startup errors.
        /// </summary>
        /// <param name="args">Command line args that should be added to the config.</param>
        /// <returns>An <see cref="IHost"/>.</returns>
        private static IHost CreateLoggingHost(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureHostConfiguration(cfg => { cfg.AddEnvironmentVariables(prefix: "ASPNETCORE_"); })
                .Build();

        /// <summary>
        /// Creates a logger factory that can be used to log startup errors.
        /// </summary>
        /// <param name="config">Config used to read app settings required for logging startup errors.</param>
        /// <returns>An <see cref="ILoggerFactory"/>.</returns>
        private static ILoggerFactory Create(IConfiguration config) =>
            LoggerFactory.Create(cfg =>
            {
                var instrumentationKey = config.GetValue<string>("ApplicationInsights:InstrumentationKey");
                cfg.AddApplicationInsights(instrumentationKey);
            });

        private static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureLogging(logging =>
                {
                    logging.AddApplicationInsights();
                })
                .ConfigureAppConfiguration((ctx, cfg) =>
                {
                    cfg.AddAzureKeyVault(new PrefixKeyVaultSecretManager("HubConfig--"));
                })
                .ConfigureWebHostDefaults(b =>
                {
                    b.UseStartup<Startup>();
                });
    }
}
