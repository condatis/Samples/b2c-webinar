﻿// <copyright file="HttpPolicies.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Services.Http
{
    using System;
    using System.Diagnostics;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Polly;
    using Polly.Extensions.Http;
    using Polly.Retry;
    using Polly.Timeout;
    using Polly.Wrap;

    /// <summary>
    /// Provides HTTP policy logic.
    /// </summary>
    public static class HttpPolicies
    {
        /// <summary>
        /// Gets the timeout and retry strategy for executing HTTP requests.
        /// </summary>
        public static AsyncPolicyWrap<HttpResponseMessage> Strategy => Policy.WrapAsync(Retry, Timeout);

        /// <summary>
        /// Gets the policy for handling HTTP timeouts.
        /// </summary>
        private static AsyncTimeoutPolicy<HttpResponseMessage> Timeout
        {
            get
            {
                return Policy.TimeoutAsync<HttpResponseMessage>(TimeSpan.FromSeconds(30), TimeoutStrategy.Optimistic, (context, timesSpan, task) =>
                {
                    Debug.WriteLine($"[Polly]: Timeout delegate fired after {timesSpan.TotalSeconds:#,##0} seconds.");
                    return Task.CompletedTask;
                });
            }
        }

        /// <summary>
        /// Gets the policy for handling HTTP retry attempts.
        /// </summary>
        private static AsyncRetryPolicy<HttpResponseMessage> Retry
        {
            get
            {
                return Policy.Handle<TimeoutRejectedException>()
                             .OrTransientHttpError()
                             .WaitAndRetryAsync(
                                                new[]
                                                {
                                                    TimeSpan.FromSeconds(1),
                                                    TimeSpan.FromSeconds(3),
                                                    TimeSpan.FromSeconds(5),
                                                }, (delegateResult, retryCount) =>
                                                {
                                                    Debug.WriteLine($"[Polly]: Retry delegate fired, attempt {retryCount}.");
                                                });
            }
        }
    }
}
