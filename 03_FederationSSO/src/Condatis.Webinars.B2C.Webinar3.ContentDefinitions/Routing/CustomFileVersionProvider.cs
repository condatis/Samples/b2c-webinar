﻿// <copyright file="CustomFileVersionProvider.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar3.ContentDefinitions.Routing
{
    using System;
    using System.Security.Cryptography;
    using Condatis.Webinars.B2C.Webinar3.ContentDefinitions.Services.Web;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc.Razor.Infrastructure;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    using Microsoft.AspNetCore.WebUtilities;
    using Microsoft.Extensions.Caching.Memory;
    using Microsoft.Extensions.FileProviders;

    /// <summary>
    /// Appends a file version to local file URLs, even if the URL is absolute rather than relative.
    /// </summary>
    public class CustomFileVersionProvider : IFileVersionProvider
    {
        private const string VersionKey = "v";
        private static readonly char[] QueryStringAndFragmentTokens = { '?', '#' };

        private readonly IMemoryCache cache;
        private readonly IFileProvider fileProvider;
        private readonly IHostNameResolver hostNameResolver;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomFileVersionProvider"/> class.
        /// </summary>
        /// <param name="hostingEnvironment">Hosting environment.</param>
        /// <param name="cacheProvider">Cache provider.</param>
        /// <param name="hostNameResolver">Host name resolver.</param>
        public CustomFileVersionProvider(IWebHostEnvironment hostingEnvironment, TagHelperMemoryCacheProvider cacheProvider, IHostNameResolver hostNameResolver)
        {
            this.hostNameResolver = hostNameResolver;
            this.fileProvider = hostingEnvironment?.WebRootFileProvider ?? throw new ArgumentNullException(nameof(hostingEnvironment));
            this.cache = cacheProvider?.Cache ?? throw new ArgumentNullException(nameof(cacheProvider));
        }

        /// <inheritdoc />
        public string AddFileVersionToPath(PathString requestPathBase, string requestedPath)
        {
            if (requestedPath == null)
            {
                throw new ArgumentNullException(nameof(requestedPath));
            }

            if (!this.TryGetFilePath(requestedPath, out var filePath))
            {
                return requestedPath;
            }

            if (this.cache.TryGetValue(requestedPath, out string pathWithHash))
            {
                return pathWithHash;
            }

            var fileInfo = this.fileProvider.GetFileInfo(filePath);
            pathWithHash = fileInfo.Exists ? QueryHelpers.AddQueryString(requestedPath, VersionKey, GetHashForFile(fileInfo)) : requestedPath;

            var cacheEntryOptions = new MemoryCacheEntryOptions();
            cacheEntryOptions.AddExpirationToken(this.fileProvider.Watch(filePath));
            cacheEntryOptions.SetSize(pathWithHash.Length * sizeof(char));

            pathWithHash = this.cache.Set(requestedPath, pathWithHash, cacheEntryOptions);

            return pathWithHash;
        }

        private static string GetHashForFile(IFileInfo fileInfo)
        {
            using var sha256 = SHA256.Create();
            using var readStream = fileInfo.CreateReadStream();

            var hash = sha256.ComputeHash(readStream);
            return WebEncoders.Base64UrlEncode(hash);
        }

        private bool TryGetFilePath(string requestedPath, out string filePath)
        {
            var requestBaseUrl = this.hostNameResolver.GetBaseUrl();

            var queryStringOrFragmentStartIndex = requestedPath.IndexOfAny(QueryStringAndFragmentTokens);
            filePath = queryStringOrFragmentStartIndex != -1 ? requestedPath.Substring(0, queryStringOrFragmentStartIndex) : requestedPath;

            if (!Uri.TryCreate(filePath, UriKind.Absolute, out var filePathUrl) || filePathUrl.IsFile)
            {
                // this is already a relative requestedPath to the file
                return true;
            }

            if (filePath.StartsWith(requestBaseUrl, StringComparison.OrdinalIgnoreCase))
            {
                // this is an absolute requestedPath to a local file so make it relative
                filePath = filePath.Substring(requestBaseUrl.Length);
                return true;
            }

            // don't append version if the requestedPath is absolute and for another site.
            filePath = requestedPath;
            return false;
        }
    }
}
