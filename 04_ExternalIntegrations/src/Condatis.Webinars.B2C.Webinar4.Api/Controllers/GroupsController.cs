﻿// <copyright file="GroupsController.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Controllers
{
    using System;
    using System.Threading.Tasks;
    using Condatis.Webinars.B2C.Webinar4.Api.Models.Groups;
    using Condatis.Webinars.B2C.Webinar4.Api.Services;
    using Condatis.Webinars.B2C.Webinar4.Api.Services.Storage;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Azure.Cosmos.Table;
    using Microsoft.Extensions.Configuration;

    /// <summary>
    /// Groups.
    /// </summary>
    [ApiController]
    public class GroupsController : B2CController
    {
        private readonly IConfiguration configuration;

        /// <summary>
        /// Initializes a new instance of the <see cref="GroupsController"/> class.
        /// </summary>
        /// <param name="modelStateConverter">Model state converter.</param>
        /// <param name="configuration">Configuration.</param>
        public GroupsController(IModelStateConverter modelStateConverter, IConfiguration configuration)
            : base(modelStateConverter)
        {
            this.configuration = configuration;
        }

        /// <summary>
        /// Get user groups.
        /// </summary>
        /// <param name="id">User ID.</param>
        /// <returns>Groups for a user.</returns>
        [Authorize(Policy = PolicyNames.ReadUserGroups)]
        [Route(RouteTypes.B2C.ControllerId)]
        public async Task<UserGroupsResponse> Get(string id)
        {
            try
            {
                var storageAccount = CloudStorageAccount.Parse(this.configuration.GetConnectionString("Storage"));
                var storageTableClient = storageAccount.CreateCloudTableClient();
                var storageTable = storageTableClient.GetTableReference("Permissions");

                var retrieveOperation = TableOperation.Retrieve<UserPermissions>(UserPermissions.PartitionKeyName, id);
                var userPermissions = await storageTable.ExecuteAsync(retrieveOperation);

                string[] groups = null;
                if (userPermissions.Result is UserPermissions user)
                {
                    groups = user.Groups?.Split(',', StringSplitOptions.RemoveEmptyEntries);
                }

                return new UserGroupsResponse
                {
                    Id = id,
                    Groups = groups ?? new[] { "user" },
                };
            }
            catch (Exception ex)
            {
                throw new B2CException(ex.Message, "Unable to read user permissions", ex);
            }
        }
    }
}
