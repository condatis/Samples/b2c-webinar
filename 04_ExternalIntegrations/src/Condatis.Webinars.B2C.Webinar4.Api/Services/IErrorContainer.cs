﻿// <copyright file="IErrorContainer.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Services
{
    /// <summary>
    /// Provides container for errors related to request data.
    /// </summary>
    public interface IErrorContainer
    {
        /// <summary>
        /// Adds an error to the container.
        /// </summary>
        /// <param name="propertyName">Name of the property the error relates to, <see cref="string.Empty"/> for errors not related to a specific property.</param>
        /// <param name="message">Error message.</param>
        void Add(string propertyName, string message);
    }
}
