﻿// <copyright file="AuthorizationRoutingMiddleware.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar3.OpenIDConnect.Services.Authorization
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Http.Features;
    using Microsoft.AspNetCore.Routing;

    /// <summary>
    /// Authorization middleware result handler.
    /// </summary>
    public class AuthorizationRoutingMiddleware
    {
        private readonly RequestDelegate next;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorizationRoutingMiddleware"/> class.
        /// </summary>
        /// <param name="next">Next middleware in the chain.</param>
        public AuthorizationRoutingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        /// <summary>
        /// Invoke.
        /// </summary>
        /// <param name="httpContext">HTTP context.</param>
        /// <param name="authService">Authorization service.</param>
        /// <returns>A <see cref="Task"/>.</returns>
        public async Task InvokeAsync(HttpContext httpContext, IAuthorizationService authService)
        {
            if (httpContext is null)
            {
                throw new ArgumentNullException(nameof(httpContext));
            }

            if (authService is null)
            {
                throw new ArgumentNullException(nameof(authService));
            }

            var endpoint = httpContext.Features.Get<IEndpointFeature>()?.Endpoint;
            var authAttr = endpoint?.Metadata?.GetMetadata<AuthorizeAttribute>();

            if (authAttr?.Policy == PolicyNames.LoA2)
            {
                var result = await authService.AuthorizeAsync(httpContext.User, httpContext.GetRouteData(), authAttr.Policy);
                if (!result.Succeeded)
                {
                    var path = $"/account/elevate?redirectUrl={httpContext.Request.Path}";
                    httpContext.Response.Redirect(path);
                    return;
                }
            }

            await this.next(httpContext);
        }
    }
}
