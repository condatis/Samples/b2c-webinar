﻿// <copyright file="SiteOptions.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar2.ContentDefinitions.Models
{
    /// <summary>
    /// Site options.
    /// </summary>
    public class SiteOptions
    {
        /// <summary>
        /// Text to display on the home page.
        /// </summary>
        public string SiteName { get; set; }
    }
}
