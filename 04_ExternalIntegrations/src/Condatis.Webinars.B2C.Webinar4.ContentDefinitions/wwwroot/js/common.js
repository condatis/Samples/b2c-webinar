﻿// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

(function($) {

    $.fn.displayInlineToBlock = function() {
        return this.each(function(i, elem) {
            var $elem = $(elem);
            if ($elem.css('display') == 'inline') {
                $elem.css('display', 'block');
            }
        });
    }

    function createObserver() {
        MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

        return new MutationObserver(function(mutations) {
            $.each(mutations, function(i, m) {
                $(m.target).displayInlineToBlock();
            });
        });
    };

    $.fn.fixVerificationAlertDisplay = function() {

        var observer = null;

        return this.each(function(i, elem) {
            observer = observer || createObserver();

            var $elem = $(elem);
            observer.observe($elem[0], {
                attributes: true,
                attributeFilter: ['style']
            });

            $elem.displayInlineToBlock();
        });
    };

    $.fn.bootstrapify = function() {
        return this.each(function(i, elem) {
            // bootstrapify page
            var $b2cContent = $('#api');

            $('> .intro', $b2cContent).addClass('h2');

            $('.card-header p', $b2cContent).addClass('mb-0');

            // bootstrapify form
            var $form = $('form', $b2cContent);
            var $formGroups = $('.entry-item, .attrEntry', $form);

            $formGroups.addClass('form-group').removeClass('entry');
            $('select,input,textarea', $formGroups).addClass('form-control');

            $('.buttons', $form).addClass('btn-group').addClass('d-block mt-3').removeClass('buttons');
            $('button, input[type="submit"], a.button', $form).addClass('btn').addClass('btn-light');

            $('.error.pageLevel', $form).addClass('alert alert-danger');
            $('.error.itemLevel', $form).addClass('text-danger');
        
            var $forgotPassword = $('#forgotPassword', $form);
            if ($forgotPassword.length > 0) {
                $('#password', $form).after($forgotPassword);
            }

            $('.divider', $form).replaceWith('<hr>');

            $('.btn#next, .btn#continue', $form).addClass('btn-primary').removeClass('btn-light');

            $('.verificationInfoText', $form).addClass('alert alert-info');
            $('.verificationSuccessText', $form).addClass('alert alert-success');
            $('.verificationErrorText', $form).addClass('alert alert-danger');

            $('.verifyButton, #signInEmail_ver_but_send', $form).addClass('btn-primary').removeClass('btn-light');

            // fix B2C forms HTML
            var $attributeList = $('#attributeList');
            if ($attributeList.length > 0) {
                $('.attrEntry', $form).appendTo($attributeList);
                $('> ul', $attributeList).remove();
            }

            // fix email verification displaying inline
            $('.verificationInfoText, .verificationSuccessText, .verificationErrorText', $form).fixVerificationAlertDisplay();
        });
    };

    $(function() {

        $('#api').bootstrapify();

        $('#continue').append($('<i class="fas fa-chevron-right"></i>'));

        // show page
        $('#body-container').removeClass('d-none').addClass('d-flex');
    });
})(jQuery);