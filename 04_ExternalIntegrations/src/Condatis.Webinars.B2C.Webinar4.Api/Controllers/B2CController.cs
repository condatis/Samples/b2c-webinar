﻿// <copyright file="B2CController.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Controllers
{
    using System;
    using System.Linq;
    using System.Net;
    using Condatis.Webinars.B2C.Webinar4.Api.Filters;
    using Condatis.Webinars.B2C.Webinar4.Api.Services;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.ModelBinding;

    /// <summary>
    /// Base controller for API calls made from B2C.
    /// </summary>
    [TypeFilter(typeof(B2CExceptionFilter))]
    public abstract class B2CController : ControllerBase
    {
        private readonly IModelStateConverter modelStateConverter;

        /// <summary>
        /// Initializes a new instance of the <see cref="B2CController"/> class.
        /// </summary>
        /// <param name="modelStateConverter">Model state converter.</param>
        protected B2CController(IModelStateConverter modelStateConverter)
        {
            this.modelStateConverter = modelStateConverter;
        }

        /// <inheritdoc />
        public override BadRequestObjectResult BadRequest(ModelStateDictionary modelState)
        {
            var modelType = this.ControllerContext.ActionDescriptor.Parameters.FirstOrDefault(p => p.BindingInfo.BindingSource.Id.Equals("Body", StringComparison.InvariantCultureIgnoreCase))?.ParameterType;
            var b2CResponse = this.modelStateConverter.ToErrorResponse(modelType, modelState);

            b2CResponse.Status = (int)HttpStatusCode.BadRequest;

            return this.BadRequest(b2CResponse);
        }

        /// <inheritdoc />
        public override ConflictObjectResult Conflict(ModelStateDictionary modelState)
        {
            var modelType = this.ControllerContext.ActionDescriptor.Parameters.FirstOrDefault(p => p.BindingInfo.BindingSource.Id.Equals("Body", StringComparison.InvariantCultureIgnoreCase))?.ParameterType;
            var b2CResponse = this.modelStateConverter.ToErrorResponse(modelType, modelState);

            b2CResponse.Status = (int)HttpStatusCode.Conflict;

            return this.Conflict(b2CResponse);
        }
    }
}
