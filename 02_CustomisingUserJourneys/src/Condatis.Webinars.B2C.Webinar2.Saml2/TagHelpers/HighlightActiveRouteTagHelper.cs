﻿// <copyright file="HighlightActiveRouteTagHelper.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar2.Saml2.TagHelpers
{
    using System;
    using System.Text.Encodings.Web;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Infrastructure;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.AspNetCore.Mvc.TagHelpers;
    using Microsoft.AspNetCore.Razor.TagHelpers;

    /// <summary>
    /// Applies the "active" class to any link that's for the current route.
    /// </summary>
    [HtmlTargetElement("a", Attributes = "[asp-highlight-active]")]
    public class HighlightActiveRouteTagHelper : TagHelper
    {
        private readonly IActionContextAccessor actionContextAccessor;
        private readonly IUrlHelperFactory urlHelperFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="HighlightActiveRouteTagHelper"/> class.
        /// </summary>
        /// <param name="actionContextAccessor">Action context accessor.</param>
        /// <param name="urlHelperFactory">URL helper.</param>
        public HighlightActiveRouteTagHelper(IActionContextAccessor actionContextAccessor, IUrlHelperFactory urlHelperFactory)
        {
            this.actionContextAccessor = actionContextAccessor;
            this.urlHelperFactory = urlHelperFactory;
        }

        /// <summary>
        /// Whether to enable link highlighting based on the active route.
        /// </summary>
        [HtmlAttributeName("asp-highlight-active")]
        public bool HighlightLink { get; set; }

        /// <inheritdoc />
        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (output is null)
            {
                throw new ArgumentNullException(nameof(output));
            }

            await base.ProcessAsync(context, output);

            if (!this.HighlightLink)
            {
                return;
            }

            if (this.MatchesPath(context))
            {
                output.AddClass("active", HtmlEncoder.Default);
            }
        }

        private bool MatchesPath(TagHelperContext context)
        {
            var actionContext = this.actionContextAccessor.ActionContext;

            ////var expectedArea = context.AllAttributes["asp-area"]?.Value?.ToString() ?? string.Empty;
            var expectedController = context.AllAttributes["asp-controller"]?.Value?.ToString() ?? string.Empty;
            var expectedAction = context.AllAttributes["asp-action"]?.Value?.ToString() ?? string.Empty;

            ////var actualArea = this.httpContextAccessor.HttpContext.Request.RouteValues["area"]?.ToString() ?? string.Empty;
            var actualController = actionContext.ActionDescriptor.RouteValues["controller"] ?? string.Empty;
            var actualAction = actionContext.ActionDescriptor.RouteValues["action"] ?? string.Empty;

            var urlHelper = this.urlHelperFactory.GetUrlHelper(actionContext);

            var expectedPath = urlHelper.Action(expectedAction, expectedController);
            var actualPath = urlHelper.Action(actualAction, actualController);

            return expectedPath.Equals(actualPath, StringComparison.OrdinalIgnoreCase);
        }
    }
}
