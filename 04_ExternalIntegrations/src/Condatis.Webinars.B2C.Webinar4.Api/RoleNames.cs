﻿// <copyright file="RoleNames.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api
{
    /// <summary>
    /// Role names.
    /// </summary>
    public static class RoleNames
    {
        /// <summary>
        /// Role to read user groups.
        /// </summary>
        public const string UserGroupsRead = "User.Groups.Read";

        /// <summary>
        /// Role to write users.
        /// </summary>
        public const string UserWrite = "User.Write";
    }
}