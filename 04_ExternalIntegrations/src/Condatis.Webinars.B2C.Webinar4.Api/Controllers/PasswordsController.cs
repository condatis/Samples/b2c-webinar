﻿// <copyright file="PasswordsController.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Condatis.Webinars.B2C.Webinar4.Api.Models.Passwords;
    using Condatis.Webinars.B2C.Webinar4.Api.Services;
    using Condatis.Webinars.B2C.Webinar4.Api.Services.Passwords;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Users.
    /// </summary>
    [ApiController]
    public class PasswordsController : B2CController
    {
        private readonly IEnumerable<IPasswordValidator> passwordValidators;

        /// <summary>
        /// Initializes a new instance of the <see cref="PasswordsController"/> class.
        /// </summary>
        /// <param name="modelStateConverter">Model state converter.</param>
        /// <param name="passwordValidators">Password validators.</param>
        public PasswordsController(IModelStateConverter modelStateConverter, IEnumerable<IPasswordValidator> passwordValidators)
            : base(modelStateConverter)
        {
            this.passwordValidators = passwordValidators;
        }

        /// <summary>
        /// Validate.
        /// </summary>
        /// <param name="request">Request.</param>
        /// <returns>Response.</returns>
        [Authorize(Policy = PolicyNames.Authorised)]
        [Route(RouteTypes.B2C.Controller + "/validate")]
        [HttpPost]
        public async Task<IActionResult> Validate(ValidatePasswordRequest request)
        {
            try
            {
                if (!this.ModelState.IsValid)
                {
                    return this.BadRequest(this.ModelState);
                }

                foreach (var passwordValidator in this.passwordValidators)
                {
                    if (!await passwordValidator.ValidateAsync(request.Password, new ModelStateErrorContainer(this.ModelState)))
                    {
                        return this.BadRequest(this.ModelState);
                    }
                }

                return this.NoContent();
            }
            catch (Exception ex)
            {
                throw new B2CException(ex.Message, "Unable to read user record.", ex);
            }
        }

        /// <summary>
        /// Validate.
        /// </summary>
        /// <param name="request">Request.</param>
        /// <returns>Response.</returns>
        [AllowAnonymous]
        [Route(RouteTypes.Default.Controller + "/validate")]
        [HttpPost]
        public async Task<IActionResult> ValidatePublic(ValidatePasswordRequest request)
        {
            if (!this.ModelState.IsValid)
            {
                return this.BadRequest(this.ModelState);
            }

            var response = new ValidatePasswordResponse();

            foreach (var passwordValidator in this.passwordValidators)
            {
                if (!await passwordValidator.ValidateAsync(request.Password, new PasswordResponseErrorContainer(response)))
                {
                    return this.Ok(response);
                }
            }

            return this.Ok(response);
        }
    }
}
