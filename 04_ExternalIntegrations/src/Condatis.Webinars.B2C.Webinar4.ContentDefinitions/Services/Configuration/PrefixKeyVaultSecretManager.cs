﻿// <copyright file="PrefixKeyVaultSecretManager.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.ContentDefinitions.Services.Configuration
{
    using System;
    using Microsoft.Azure.KeyVault.Models;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Configuration.AzureKeyVault;

    /// <summary>
    /// Provides a way to load configuration secrets from Key Vault with a particular prefix.
    /// </summary>
    public class PrefixKeyVaultSecretManager : IKeyVaultSecretManager
    {
        private readonly string prefix;

        /// <summary>
        /// Initializes a new instance of the <see cref="PrefixKeyVaultSecretManager"/> class.
        /// </summary>
        /// <param name="prefix">Prefix.</param>
        public PrefixKeyVaultSecretManager(string prefix)
        {
            this.prefix = prefix;
        }

        /// <inheritdoc />
        public bool Load(SecretItem secret)
        {
            return secret?.Identifier.Name
                         .StartsWith(this.prefix, StringComparison.InvariantCultureIgnoreCase) == true;
        }

        /// <inheritdoc />
        public string GetKey(SecretBundle secret)
        {
            return secret?.SecretIdentifier.Name
                         .Substring(this.prefix.Length)
                         .Replace("--", ConfigurationPath.KeyDelimiter, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
