﻿// <copyright file="SlugifyParameterTransformer.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Routing
{
    using System.Text.RegularExpressions;
    using Microsoft.AspNetCore.Routing;

    /// <summary>
    /// Slugifies route parameters.
    /// </summary>
    public class SlugifyParameterTransformer : IOutboundParameterTransformer
    {
        /// <inheritdoc />
        public string TransformOutbound(object value)
        {
            if (value == null)
            {
                return null;
            }

            // Slugify value
#pragma warning disable CA1308 // Normalize strings to uppercase
            return Regex.Replace(value.ToString(), "([a-z])([A-Z])", "$1-$2").ToLowerInvariant();
#pragma warning restore CA1308 // Normalize strings to uppercase
        }
    }
}
