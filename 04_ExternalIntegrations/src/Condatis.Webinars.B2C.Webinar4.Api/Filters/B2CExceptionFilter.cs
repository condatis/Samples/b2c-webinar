﻿// <copyright file="B2CExceptionFilter.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Filters
{
    using System;
    using System.Net;
    using Condatis.Webinars.B2C.Webinar4.Api.Models;
    using Condatis.Webinars.B2C.Webinar4.Api.Services;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Filters;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// Handles B2C exceptions thrown when executing a request.
    /// </summary>
    public class B2CExceptionFilter : IExceptionFilter
    {
        private readonly IVersionNumberAccessor versionNumberAccessor;
        private readonly ILogger<B2CExceptionFilter> logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="B2CExceptionFilter"/> class.
        /// </summary>
        /// <param name="versionNumberAccessor">Version number accessor.</param>
        /// <param name="logger">Logger.</param>
        public B2CExceptionFilter(IVersionNumberAccessor versionNumberAccessor, ILogger<B2CExceptionFilter> logger)
        {
            this.versionNumberAccessor = versionNumberAccessor;
            this.logger = logger;
        }

        /// <inheritdoc />
        public void OnException(ExceptionContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            this.logger.LogError(context.Exception.Message, context.Exception);

            var userMessage = "Sorry, an error occurred!";
            var developerMessage = context.Exception?.Message;

            if (context.Exception is B2CException b2cException)
            {
                userMessage = b2cException.UserMessage;
            }

            context.ExceptionHandled = true;
            context.Result = new ConflictObjectResult(new B2CErrorResponse
            {
                Version = this.versionNumberAccessor.GetVersionNumber(),
                Status = (int)HttpStatusCode.Conflict,
                DeveloperMessage = developerMessage,
                UserMessage = userMessage,
                RequestId = context.HttpContext.TraceIdentifier,
            });
        }
    }
}
