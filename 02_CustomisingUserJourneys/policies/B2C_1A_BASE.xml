<!-- 
  Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
  Licensed under the MIT license. See LICENSE file in the project root for full license information.
-->

<TrustFrameworkPolicy
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema"
  xmlns="http://schemas.microsoft.com/online/cpim/schemas/2013/06"
  PolicySchemaVersion="0.3.0.0"
  TenantId="#{Policy.B2C.TenantDomain}#"
  PolicyId="#{Policy.PolicyId.Base}#"
  PublicPolicyUri="#{Policy.PublicPolicyUri}#/#{Policy.PolicyId.Base}#"
  DeploymentMode="#{Policy.DeploymentMode}#"
  TenantObjectId="#{Policy.B2C.TenantId}#"
  >
  <BuildingBlocks>
    <ClaimsSchema>
      <ClaimType Id="signInEmail">
        <DisplayName>Email address</DisplayName>
        <DataType>string</DataType>
        <UserHelpText/>
        <UserInputType>EmailBox</UserInputType>
        <Restriction>
          <!--Email pattern taken from http://emailregex.com/ -->
          <Pattern RegularExpression="^([a-zA-Z0-9_\-\.!#$%&amp;'*+\/=?^_`.{|}~]+)@([a-zA-Z\d][a-zA-Z\d\-]*(?:\.[a-zA-Z\d][a-zA-Z\d\-]*)*|\[(?:(?:2(?:[0-4][0-9]|5[0-5])|[0-1]?[0-9]?[0-9])\.){3}(?:(?:2([0-4][0-9]|5[0-5])|[0-1]?[0-9]?[0-9]))\])$" HelpText="Enter a valid e-mail address" />
        </Restriction>
      </ClaimType>
      <ClaimType Id="signInPassword">
        <DisplayName>Password</DisplayName>
        <DataType>string</DataType>
        <AdminHelpText>Password</AdminHelpText>
        <UserHelpText>Password</UserHelpText>
        <UserInputType>TextBox</UserInputType>
      </ClaimType>
      <ClaimType Id="displayName">
        <DisplayName>Display name</DisplayName>
        <DataType>string</DataType>
        <DefaultPartnerClaimTypes>
          <Protocol Name="OAuth2" PartnerClaimType="unique_name" />
          <Protocol Name="OpenIdConnect" PartnerClaimType="name" />
          <Protocol Name="SAML2" PartnerClaimType="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name" />
        </DefaultPartnerClaimTypes>
        <AdminHelpText />
        <UserHelpText>Your display name.</UserHelpText>
        <UserInputType>TextBox</UserInputType>
      </ClaimType>
      <ClaimType Id="givenName">
        <DisplayName>First name</DisplayName>
        <DataType>string</DataType>
        <DefaultPartnerClaimTypes>
          <Protocol Name="OAuth2" PartnerClaimType="given_name" />
          <Protocol Name="OpenIdConnect" PartnerClaimType="given_name" />
          <Protocol Name="SAML2" PartnerClaimType="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname" />
        </DefaultPartnerClaimTypes>
        <AdminHelpText>The user's given name (also known as first name).</AdminHelpText>
        <UserHelpText>Your given name (also known as first name).</UserHelpText>
        <UserInputType>TextBox</UserInputType>
      </ClaimType>
      <ClaimType Id="surname">
        <DisplayName>Last name</DisplayName>
        <DataType>string</DataType>
        <DefaultPartnerClaimTypes>
          <Protocol Name="OAuth2" PartnerClaimType="family_name" />
          <Protocol Name="OpenIdConnect" PartnerClaimType="family_name" />
          <Protocol Name="SAML2" PartnerClaimType="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname" />
        </DefaultPartnerClaimTypes>
        <AdminHelpText>The user's surname (also known as family name or last name).</AdminHelpText>
        <UserHelpText>Your surname (also known as family name or last name).</UserHelpText>
        <UserInputType>TextBox</UserInputType>
      </ClaimType>
      <ClaimType Id="newEmail">
        <DisplayName>New email address</DisplayName>
        <DataType>string</DataType>
        <UserHelpText/>
        <UserInputType>EmailBox</UserInputType>
        <Restriction>
          <!--Email pattern taken from http://emailregex.com/ -->
          <Pattern RegularExpression="^([a-zA-Z0-9_\-\.!#$%&amp;'*+\/=?^_`.{|}~]+)@([a-zA-Z\d][a-zA-Z\d\-]*(?:\.[a-zA-Z\d][a-zA-Z\d\-]*)*|\[(?:(?:2(?:[0-4][0-9]|5[0-5])|[0-1]?[0-9]?[0-9])\.){3}(?:(?:2([0-4][0-9]|5[0-5])|[0-1]?[0-9]?[0-9]))\])$" HelpText="Enter a valid e-mail address" />
        </Restriction>
      </ClaimType>
      <ClaimType Id="UserId">
        <DisplayName>UserID</DisplayName>
        <DataType>string</DataType>
        <AdminHelpText />
        <UserHelpText />
        <UserInputType>TextBox</UserInputType>
      </ClaimType>
      <ClaimType Id="signInName">
        <DisplayName>Sign in name</DisplayName>
        <DataType>string</DataType>
        <UserHelpText />
        <UserInputType>TextBox</UserInputType>
      </ClaimType>
      <ClaimType Id="tenantId">
        <DisplayName>User's Object's Tenant ID</DisplayName>
        <DataType>string</DataType>
        <DefaultPartnerClaimTypes>
          <Protocol Name="OAuth2" PartnerClaimType="tid" />
          <Protocol Name="OpenIdConnect" PartnerClaimType="tid" />
          <Protocol Name="SAML2" PartnerClaimType="http://schemas.microsoft.com/identity/claims/tenantid" />
        </DefaultPartnerClaimTypes>
        <AdminHelpText>Tenant identifier (ID) of the user object in Azure AD.</AdminHelpText>
        <UserHelpText>Tenant identifier (ID) of the user object in Azure AD.</UserHelpText>
      </ClaimType>
      <ClaimType Id="objectId">
        <DisplayName>User's Object ID</DisplayName>
        <DataType>string</DataType>
        <DefaultPartnerClaimTypes>
          <Protocol Name="OAuth2" PartnerClaimType="oid" />
          <Protocol Name="OpenIdConnect" PartnerClaimType="oid" />
          <Protocol Name="SAML2" PartnerClaimType="http://schemas.microsoft.com/identity/claims/objectidentifier" />
        </DefaultPartnerClaimTypes>
        <AdminHelpText>Object identifier (ID) of the user object in Azure AD.</AdminHelpText>
        <UserHelpText>Object identifier (ID) of the user object in Azure AD.</UserHelpText>
      </ClaimType>
      <ClaimType Id="password">
        <DisplayName>Password</DisplayName>
        <DataType>string</DataType>
        <AdminHelpText>Password</AdminHelpText>
        <UserHelpText>Password</UserHelpText>
        <UserInputType>Password</UserInputType>
        <Restriction>
          <Pattern RegularExpression="^((?=.*[a-z])(?=.*[A-Z])(?=.*\d)|(?=.*[a-z])(?=.*[A-Z])(?=.*[^A-Za-z0-9])|(?=.*[a-z])(?=.*\d)(?=.*[^A-Za-z0-9])|(?=.*[A-Z])(?=.*\d)(?=.*[^A-Za-z0-9]))([A-Za-z\d@#$%^&amp;*\-_+=[\]{}|\\:',?/`~&quot;();!]|\.(?!@)){8,16}$" HelpText="8-16 characters, containing 3 out of 4 of the following: Lowercase characters, uppercase characters, digits (0-9), and one or more of the following symbols: @ # $ % ^ &amp; * - _ + = [ ] { } | \ : ' , ? / ` ~ &quot; ( ) ; ." />
        </Restriction>
      </ClaimType>
      <ClaimType Id="reenterPassword">
        <DisplayName>Re-enter new password</DisplayName>
        <DataType>string</DataType>
        <AdminHelpText>Re enter new password</AdminHelpText>
        <UserHelpText>Re enter new password</UserHelpText>
        <UserInputType>Password</UserInputType>
        <Restriction>
          <Pattern RegularExpression="^((?=.*[a-z])(?=.*[A-Z])(?=.*\d)|(?=.*[a-z])(?=.*[A-Z])(?=.*[^A-Za-z0-9])|(?=.*[a-z])(?=.*\d)(?=.*[^A-Za-z0-9])|(?=.*[A-Z])(?=.*\d)(?=.*[^A-Za-z0-9]))([A-Za-z\d@#$%^&amp;*\-_+=[\]{}|\\:',?/`~&quot;();!]|\.(?!@)){8,16}$" HelpText=" " />
        </Restriction>
      </ClaimType>
      <ClaimType Id="sub">
        <DisplayName>Subject</DisplayName>
        <DataType>string</DataType>
        <DefaultPartnerClaimTypes>
          <Protocol Name="OpenIdConnect" PartnerClaimType="sub" />
          <Protocol Name="SAML2" PartnerClaimType="sub" />
        </DefaultPartnerClaimTypes>
        <AdminHelpText />
        <UserHelpText />
      </ClaimType>
      <ClaimType Id="alternativeSecurityId">
        <DisplayName>AlternativeSecurityId</DisplayName>
        <DataType>string</DataType>
        <AdminHelpText />
        <UserHelpText />
      </ClaimType>
      <ClaimType Id="identityProvider">
        <DisplayName>Identity Provider</DisplayName>
        <DataType>string</DataType>
        <DefaultPartnerClaimTypes>
          <Protocol Name="OAuth2" PartnerClaimType="idp" />
          <Protocol Name="OpenIdConnect" PartnerClaimType="http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider" />
          <Protocol Name="SAML2" PartnerClaimType="http://schemas.microsoft.com/identity/claims/identityprovider" />
        </DefaultPartnerClaimTypes>
        <AdminHelpText />
        <UserHelpText />
      </ClaimType>
      <ClaimType Id="mobile">
        <DisplayName>Mobile phone</DisplayName>
        <DataType>string</DataType>
        <Mask Type="Simple">XXX-XXX-</Mask>
        <AdminHelpText />
        <UserHelpText>Your mobile phone number.</UserHelpText>
      </ClaimType>
      <ClaimType Id="Verified.mobile">
        <DisplayName>Mobile phone</DisplayName>
        <DataType>string</DataType>
        <Mask Type="Simple">XXX-XXX-</Mask>
        <AdminHelpText />
        <UserHelpText>Your mobile phone number.</UserHelpText>
      </ClaimType>
      <ClaimType Id="newMobile">
        <DisplayName>Mobile phone</DisplayName>
        <DataType>string</DataType>
        <Mask Type="Simple">XXX-XXX-</Mask>
        <AdminHelpText />
        <UserHelpText>Your mobile phone number.</UserHelpText>
        <Restriction>
          <Pattern RegularExpression="^\d{5,20}$" HelpText="Your password must have a minimum of 8 characters, maximum of 16 characters and contain at least 3 of the following : lowercase characters, uppercase characters, numbers (0-9), symbols" />
        </Restriction>
      </ClaimType>
      <ClaimType Id="email">
        <DisplayName>Work E-mail address</DisplayName>
        <DataType>string</DataType>
        <DefaultPartnerClaimTypes>
          <Protocol Name="OpenIdConnect" PartnerClaimType="email" />
        </DefaultPartnerClaimTypes>
        <AdminHelpText />
        <UserHelpText><![CDATA[Your work e-mail address is required for us to send important notifications and alerts.]]></UserHelpText>
        <UserInputType>TextBox</UserInputType>
        <Restriction>
          <!--Email pattern taken from http://emailregex.com/ -->
          <Pattern RegularExpression="^([a-zA-Z0-9_\-\.!#$%&amp;'*+\/=?^_`.{|}~]+)@([a-zA-Z\d][a-zA-Z\d\-]*(?:\.[a-zA-Z\d][a-zA-Z\d\-]*)*|\[(?:(?:2(?:[0-4][0-9]|5[0-5])|[0-1]?[0-9]?[0-9])\.){3}(?:(?:2([0-4][0-9]|5[0-5])|[0-1]?[0-9]?[0-9]))\])$"
                   HelpText="Enter a valid e-mail address" />
        </Restriction>
      </ClaimType>
      <ClaimType Id="emailDomain">
        <DisplayName>Work E-mail address domain name</DisplayName>
        <DataType>string</DataType>
      </ClaimType>
      <ClaimType Id="userPrincipalName">
        <DisplayName>UserPrincipalName</DisplayName>
        <DataType>string</DataType>
        <DefaultPartnerClaimTypes>
          <Protocol Name="OAuth2" PartnerClaimType="upn" />
          <Protocol Name="OpenIdConnect" PartnerClaimType="upn" />
          <Protocol Name="SAML2" PartnerClaimType="http://schemas.microsoft.com/identity/claims/userprincipalname" />
        </DefaultPartnerClaimTypes>
        <AdminHelpText>The user name as stored in the Azure Active Directory.</AdminHelpText>
        <UserHelpText>Your user name as stored in the Azure Active Directory.</UserHelpText>
      </ClaimType>
      <ClaimType Id="authenticationSource">
        <DisplayName>AuthenticationSource</DisplayName>
        <DataType>string</DataType>
        <AdminHelpText>Specifies whether the user was authenticated at Social IDP or EvoSTS or local account.</AdminHelpText>
        <UserHelpText>Specifies whether the user was authenticated at Social IDP or EvoSTS or local account.</UserHelpText>
      </ClaimType>
      <ClaimType Id="passwordPolicies">
        <DisplayName>Password Policies</DisplayName>
        <DataType>string</DataType>
        <AdminHelpText>Password policies used by MSODS to determine password strength, expiry etc.</AdminHelpText>
        <UserHelpText>Password policies used by MSODS to determine password strength, expiry etc.</UserHelpText>
      </ClaimType>
      <ClaimType Id="amr">
        <DisplayName>amr</DisplayName>
        <DataType>string</DataType>
        <AdminHelpText>Authentication methods requested.</AdminHelpText>
        <UserHelpText>Authentication methods requested.</UserHelpText>
      </ClaimType>
      <ClaimType Id="nca">
        <DisplayName>nca</DisplayName>
        <DataType>string</DataType>
        <AdminHelpText>Special parameter passed to EvoSTS.</AdminHelpText>
        <UserHelpText>Special parameter passed to EvoSTS.</UserHelpText>
      </ClaimType>
      <ClaimType Id="grant_type">
        <DisplayName>grant_type</DisplayName>
        <DataType>string</DataType>
        <AdminHelpText>Special parameter passed to EvoSTS.</AdminHelpText>
        <UserHelpText>Special parameter passed to EvoSTS.</UserHelpText>
      </ClaimType>
      <ClaimType Id="scope">
        <DisplayName>scope</DisplayName>
        <DataType>string</DataType>
        <AdminHelpText>Special parameter passed to EvoSTS.</AdminHelpText>
        <UserHelpText>Special parameter passed to EvoSTS.</UserHelpText>
      </ClaimType>
      <ClaimType Id="client_id">
        <DisplayName>client_id</DisplayName>
        <DataType>string</DataType>
        <AdminHelpText>Special parameter passed to EvoSTS.</AdminHelpText>
        <UserHelpText>Special parameter passed to EvoSTS.</UserHelpText>
      </ClaimType>
      <ClaimType Id="resource_id">
        <DisplayName>resource_id</DisplayName>
        <DataType>string</DataType>
        <AdminHelpText>Special parameter passed to EvoSTS.</AdminHelpText>
        <UserHelpText>Special parameter passed to EvoSTS.</UserHelpText>
      </ClaimType>
      <ClaimType Id="objectIdFromSession">
        <DisplayName>objectIdFromSession</DisplayName>
        <DataType>boolean</DataType>
        <UserHelpText>Parameter provided by the default session management provider to indicate that the object id has been retrieved from an SSO session.</UserHelpText>
      </ClaimType>
      <ClaimType Id="pageMessage">
        <DataType>string</DataType>
        <UserInputType>Paragraph</UserInputType>
      </ClaimType>
    </ClaimsSchema>

    <ClaimsTransformations>
      <ClaimsTransformation Id="CreateAlternativeSecurityId" TransformationMethod="CreateAlternativeSecurityId">
        <InputClaims>
          <InputClaim ClaimTypeReferenceId="userId" TransformationClaimType="key" />
          <InputClaim ClaimTypeReferenceId="identityProvider" TransformationClaimType="identityProvider" />
        </InputClaims>
        <OutputClaims>
          <OutputClaim ClaimTypeReferenceId="alternativeSecurityId" TransformationClaimType="alternativeSecurityId" />
        </OutputClaims>
      </ClaimsTransformation>
      <ClaimsTransformation Id="GetEmailDomain" TransformationMethod="ParseDomain">
        <InputClaims>
          <InputClaim ClaimTypeReferenceId="signInEmail" TransformationClaimType="emailAddress"/>
        </InputClaims>
        <OutputClaims>
          <OutputClaim ClaimTypeReferenceId="emailDomain" TransformationClaimType="domain"/>
        </OutputClaims>
      </ClaimsTransformation>
      <ClaimsTransformation Id="GetNewEmailDomain" TransformationMethod="ParseDomain">
        <InputClaims>
          <InputClaim ClaimTypeReferenceId="newEmail" TransformationClaimType="emailAddress"/>
        </InputClaims>
        <OutputClaims>
          <OutputClaim ClaimTypeReferenceId="emailDomain" TransformationClaimType="domain"/>
        </OutputClaims>
      </ClaimsTransformation>
    </ClaimsTransformations>

    <ClientDefinitions>
      <ClientDefinition Id="DefaultWeb">
        <ClientUIFilterFlags>LineMarkers, MetaRefresh</ClientUIFilterFlags>
      </ClientDefinition>
    </ClientDefinitions>

    <ContentDefinitions>
      <ContentDefinition Id="content.generic">
        <LoadUri>~/tenant/default/selfAsserted.cshtml</LoadUri>
        <RecoveryUri>~/common/default_page_error.html</RecoveryUri>
        <DataUri>urn:com:microsoft:aad:b2c:elements:contract:selfasserted:1.2.0</DataUri>
      </ContentDefinition>
      <ContentDefinition Id="content.signin">
        <LoadUri>~/tenant/default/unified.cshtml</LoadUri>
        <RecoveryUri>~/common/default_page_error.html</RecoveryUri>
        <DataUri>urn:com:microsoft:aad:b2c:elements:contract:unifiedssp:1.2.0</DataUri>
      </ContentDefinition>
      <ContentDefinition Id="content.signup">
        <LoadUri>~/tenant/default/selfAsserted.cshtml</LoadUri>
        <RecoveryUri>~/common/default_page_error.html</RecoveryUri>
        <DataUri>urn:com:microsoft:aad:b2c:elements:contract:selfasserted:1.2.0</DataUri>
      </ContentDefinition>
    </ContentDefinitions>

  </BuildingBlocks>

  <ClaimsProviders>

    <ClaimsProvider>      
      <DisplayName>Token Issuer</DisplayName>
      <TechnicalProfiles>
        <TechnicalProfile Id="JwtIssuer">
          <DisplayName>JWT Issuer</DisplayName>
          <Protocol Name="OpenIdConnect" />
          <OutputTokenFormat>JWT</OutputTokenFormat>
          <Metadata>
            <Item Key="client_id">{service:te}</Item>
            <Item Key="id_token_lifetime_secs">900</Item>
            <Item Key="issuer_refresh_token_user_identity_claim_type">objectId</Item>
          </Metadata>
          <CryptographicKeys>
            <Key Id="issuer_secret" StorageReferenceId="B2C_1A_TokenSigningKeyContainer" />
            <Key Id="issuer_refresh_token_key" StorageReferenceId="B2C_1A_TokenEncryptionKeyContainer" />
          </CryptographicKeys>
          <InputClaims />
          <OutputClaims />
          <UseTechnicalProfileForSessionManagement ReferenceId="SM-OAuth-Issuer" />
        </TechnicalProfile>   

        <TechnicalProfile Id="SM-OAuth-Issuer">
          <DisplayName>Session Management Provider</DisplayName>
          <Protocol Name="Proprietary" Handler="Web.TPEngine.SSO.OAuthSSOSessionProvider, Web.TPEngine, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" />
        </TechnicalProfile>  
      </TechnicalProfiles>
    </ClaimsProvider>

    <ClaimsProvider>
      <DisplayName>Trust-framework Policy Engine TechnicalProfiles</DisplayName>
      <TechnicalProfiles>
        <TechnicalProfile Id="TpEngine_c3bd4fe2-1775-4013-b91d-35f16d377d13">
          <DisplayName>Trust-framework Policy Engine Default Technical Profile</DisplayName>
          <Protocol Name="None" />
          <Metadata>
            <Item Key="url">{service:te}</Item>
          </Metadata>
        </TechnicalProfile>
      </TechnicalProfiles>
    </ClaimsProvider>

  </ClaimsProviders>

</TrustFrameworkPolicy>