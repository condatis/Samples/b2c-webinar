﻿// <copyright file="PwnedPasswordValidator.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Services.Passwords
{
    using System;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading.Tasks;
    using Flurl.Http;
    using Flurl.Http.Configuration;
    using Humanizer;
    using Microsoft.ApplicationInsights;
    using Microsoft.ApplicationInsights.DataContracts;

    /// <summary>
    /// Provides a validator that uses Have I Been Pwned to check whether the password has been in any data breaches.
    /// </summary>
    public class PwnedPasswordValidator : IPasswordValidator
    {
        private readonly TelemetryClient telemetryClient;
        private readonly IFlurlClient flurlClient;

        /// <summary>
        /// Initializes a new instance of the <see cref="PwnedPasswordValidator"/> class.
        /// </summary>
        /// <param name="flurlClientFactory">FlurlClient factory.</param>
        /// <param name="telemetryClient">Telemetry client.</param>
        public PwnedPasswordValidator(IFlurlClientFactory flurlClientFactory, TelemetryClient telemetryClient)
        {
            this.telemetryClient = telemetryClient;
            this.flurlClient = flurlClientFactory.Get("https://api.pwnedpasswords.com/range/");
        }

        /// <inheritdoc />
        public async Task<bool> ValidateAsync(string password, IErrorContainer errorContainer)
        {
            var operation = this.telemetryClient.StartOperation<DependencyTelemetry>("Have I Been Pwnd password validation");

            try
            {
#pragma warning disable SCS0006 // Weak hashing function - hashing function mandated by API - https://haveibeenpwned.com/API/v3#PwnedPasswords
                var hashedPassword = BitConverter.ToString(SHA1.Create().ComputeHash(Encoding.UTF8.GetBytes(password))).Replace("-", string.Empty);
#pragma warning restore SCS0006 // Weak hashing function

                var prefix = hashedPassword.Substring(0, 5);
                var suffix = hashedPassword.Substring(5, hashedPassword.Length - 5);

                var responseStream = await this.flurlClient.Request(prefix).WithHeader("Add-Padding", true).GetStreamAsync();
                using var streamReader = new StreamReader(responseStream);

                string currentSuffix;
                while (!((currentSuffix = await streamReader.ReadLineAsync()) is null))
                {
                    var parts = currentSuffix.Split(':', StringSplitOptions.RemoveEmptyEntries);
                    if (!int.TryParse(parts[1], out var useCount))
                    {
                        // unable to read number of usages
                        continue;
                    }

                    if (useCount == 0)
                    {
                        // this result line is response padding, ignore (https://haveibeenpwned.com/API/v3#PwnedPasswordsPadding)
                        continue;
                    }

                    if (parts[0].Equals(suffix, StringComparison.OrdinalIgnoreCase))
                    {
                        errorContainer.Add(string.Empty, $"Your chosen password has appeared in {"breach".ToQuantity(useCount, "N0")}, please choose another one.");
                        return false;
                    }
                }

                return true;
            }
            finally
            {
                this.telemetryClient.StopOperation(operation);
            }
        }
    }
}
