// <copyright file="ErrorViewModel.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar3.ContentDefinitions.Models
{
    /// <summary>
    /// Provides error information to a view.
    /// </summary>
    public class ErrorViewModel
    {
        /// <summary>
        /// Gets or sets the unique id of the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// Gets or sets whether to show the <see cref="RequestId"/>.
        /// </summary>
        public bool ShowRequestId => !string.IsNullOrEmpty(this.RequestId);
    }
}
