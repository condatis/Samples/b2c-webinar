﻿// <copyright file="IModelStateConverter.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Services
{
    using System;
    using Condatis.Webinars.B2C.Webinar4.Api.Models;
    using Microsoft.AspNetCore.Mvc.ModelBinding;

    /// <summary>
    /// Provides a way to convert a <see cref="ModelStateDictionary"/> to a <see cref="B2CErrorResponse"/>.
    /// </summary>
    public interface IModelStateConverter
    {
        /// <summary>
        /// Converts a <see cref="ModelStateDictionary"/> to a <see cref="B2CErrorResponse"/>.
        /// </summary>
        /// <param name="modelType">Type of model the <see cref="ModelStateDictionary"/> represents.</param>
        /// <param name="modelState"><see cref="ModelStateDictionary"/> containing errors to convert.</param>
        /// <returns>A <see cref="B2CErrorResponse"/>.</returns>
        B2CErrorResponse ToErrorResponse(Type modelType, ModelStateDictionary modelState);

        /// <summary>
        /// Converts a <see cref="ModelStateDictionary"/> to a <see cref="B2CErrorResponse"/>.
        /// </summary>
        /// <param name="userMessage">An error message than can be safely displayed to the user.</param>
        /// <param name="modelType">Type of model the <see cref="ModelStateDictionary"/> represents.</param>
        /// <param name="modelState"><see cref="ModelStateDictionary"/> containing errors to convert.</param>
        /// <returns>A <see cref="B2CErrorResponse"/>.</returns>
        B2CErrorResponse ToErrorResponse(string userMessage, Type modelType, ModelStateDictionary modelState);
    }
}