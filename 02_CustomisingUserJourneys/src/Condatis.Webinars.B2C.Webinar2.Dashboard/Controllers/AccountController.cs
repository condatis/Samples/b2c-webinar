﻿// <copyright file="AccountController.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar2.Dashboard.Controllers
{
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authentication;
    using Microsoft.AspNetCore.Authentication.Cookies;
    using Microsoft.AspNetCore.Authentication.OpenIdConnect;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Account controller.
    /// </summary>
    public class AccountController : Controller
    {
        /// <summary>
        /// Sign out.
        /// </summary>
        /// <param name="scheme">Scheme to sign out of.</param>
        /// <returns>An <see cref="IActionResult"/>.</returns>
        public IActionResult SignOut(string scheme)
        {
            scheme ??= OpenIdConnectDefaults.AuthenticationScheme;
            var callbackUrl = this.Url.Action("SignedOut", "Account");
            return this.SignOut(new AuthenticationProperties { RedirectUri = callbackUrl, }, CookieAuthenticationDefaults.AuthenticationScheme, scheme);
        }

        /// <summary>
        /// End session.
        /// </summary>
        /// <returns>An <see cref="IActionResult"/>.</returns>
        public async Task<IActionResult> EndSession()
        {
            await this.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return this.Ok();
        }

        /// <summary>
        /// Signed out.
        /// </summary>
        /// <returns>An <see cref="IActionResult"/>.</returns>
        public IActionResult SignedOut()
        {
            return this.RedirectToAction("Index", "Home");
        }
    }
}
