﻿// <copyright file="UsersController.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Controllers
{
    using System;
    using System.Data.SqlClient;
    using System.Threading.Tasks;
    using Condatis.Webinars.B2C.Webinar4.Api.Models.Users;
    using Condatis.Webinars.B2C.Webinar4.Api.Services;
    using Condatis.Webinars.B2C.Webinar4.Api.Services.Sql;
    using Dapper;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;

    /// <summary>
    /// Users.
    /// </summary>
    [ApiController]
    public class UsersController : B2CController
    {
        private readonly IConfiguration configuration;

        /// <summary>
        /// Initializes a new instance of the <see cref="UsersController"/> class.
        /// </summary>
        /// <param name="modelStateConverter">Model state converter.</param>
        /// <param name="configuration">Configuration.</param>
        public UsersController(IModelStateConverter modelStateConverter, IConfiguration configuration)
            : base(modelStateConverter)
        {
            this.configuration = configuration;
        }

        /// <summary>
        /// Upsert user.
        /// </summary>
        /// <param name="request">Request.</param>
        /// <returns>Response.</returns>
        [Authorize(Policy = PolicyNames.WriteUser)]
        [Route(RouteTypes.B2C.Controller)]
        [HttpPost]
        public async Task<IActionResult> Post(PostUserRequest request)
        {
            const string selectUserSql = @"SELECT U.Id, U.Forename, U.Surname FROM Users U WHERE U.Id = @Id";
            const string insertUserSql = @"INSERT INTO Users ( Id, Forename, Surname ) VALUES ( @Id, @Forename, @Surname )";
            const string updateUserSql = @"UPDATE U SET U.Forename = @Forename, U.Surname = @Surname FROM Users U WHERE U.Id = @Id";

            try
            {
                if (!this.ModelState.IsValid)
                {
                    return this.BadRequest(this.ModelState);
                }

                await using var connection = new SqlConnection(this.configuration.GetConnectionString("Sql"));
                await connection.OpenAsync();

                var existingUser = await connection.QueryFirstOrDefaultAsync<SqlUser>(selectUserSql, new { Id = request.Id });
                if (existingUser is null)
                {
                    var createdUser = new SqlUser
                    {
                        Id = request.Id,
                        Forename = request.Forename,
                        Surname = request.Surname,
                    };

                    await connection.ExecuteAsync(insertUserSql, createdUser);

                    return this.CreatedAtAction("Get", new { id = createdUser.Id }, createdUser);
                }
                else
                {
                    existingUser.Forename = request.Forename ?? existingUser.Forename;
                    existingUser.Surname = request.Surname ?? existingUser.Surname;

                    await connection.ExecuteAsync(updateUserSql, existingUser);

                    return this.Ok(existingUser);
                }
            }
            catch (Exception ex)
            {
                throw new B2CException(ex.Message, "Unable to save user record.", ex);
            }
        }

        /// <summary>
        /// Get user.
        /// </summary>
        /// <param name="id">User ID.</param>
        /// <returns>Response.</returns>
        [Authorize(Policy = PolicyNames.Authorised)]
        [Route(RouteTypes.B2C.ControllerId)]
        public async Task<IActionResult> Get(Guid id)
        {
            const string selectUserSql = @"SELECT U.Id, U.Forename, U.Surname FROM Users U WHERE U.Id = @Id";

            try
            {
                await using var connection = new SqlConnection(this.configuration.GetConnectionString("Sql"));
                await connection.OpenAsync();

                var existingUser = await connection.QueryFirstOrDefaultAsync<SqlUser>(selectUserSql, new { Id = id });
                if (existingUser is null)
                {
                    throw new NotFoundException($"Unable to find user '{id}'.");
                }

                return this.Ok(existingUser);
            }
            catch (Exception ex)
            {
                throw new B2CException(ex.Message, "Unable to read user record.", ex);
            }
        }
    }
}
