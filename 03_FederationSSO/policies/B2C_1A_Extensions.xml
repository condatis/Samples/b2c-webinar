<!-- 
  Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
  Licensed under the MIT license. See LICENSE file in the project root for full license information.
-->

<TrustFrameworkPolicy
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema"
  xmlns="http://schemas.microsoft.com/online/cpim/schemas/2013/06"
  PolicySchemaVersion="0.3.0.0"
  TenantId="#{Policy.B2C.TenantDomain}#"
  PolicyId="#{Policy.PolicyId.Extensions}#"
  PublicPolicyUri="#{Policy.PublicPolicyUri}#/#{Policy.PolicyId.Extensions}#"
  DeploymentMode="#{Policy.DeploymentMode}#"
  TenantObjectId="#{Policy.B2C.TenantId}#"
  >
  <BasePolicy>
    <TenantId>#{Policy.B2C.TenantDomain}#</TenantId>
    <PolicyId>#{Policy.PolicyId.Base}#</PolicyId>
  </BasePolicy>

  <BuildingBlocks>
  
    <ClaimsSchema>

      <ClaimType Id="userIdForMFA">
        <DisplayName>UserId for MFA</DisplayName>
        <DataType>string</DataType>
      </ClaimType>

      <ClaimType Id="strongAuthenticationPhoneNumber">
        <DisplayName>Phone Number</DisplayName>
        <DataType>string</DataType>
        <Mask Type="Simple">XXX-XXX-</Mask>
        <UserHelpText>Your telephone number</UserHelpText>
      </ClaimType>

      <ClaimType Id="Verified.strongAuthenticationPhoneNumber">
        <DisplayName>Verified Phone Number</DisplayName>
        <DataType>string</DataType>
        <DefaultPartnerClaimTypes>
          <Protocol Name="OpenIdConnect" PartnerClaimType="phone_number" />
        </DefaultPartnerClaimTypes>
        <Mask Type="Simple">XXX-XXX-</Mask>
        <UserHelpText>Your office phone number that has been verified</UserHelpText>
      </ClaimType>

      <ClaimType Id="idp">
        <DisplayName>Identity Provider</DisplayName>
        <DataType>string</DataType>
      </ClaimType>
      <ClaimType Id="extension_referrer">
        <DisplayName>Where did you hear about us?</DisplayName>
        <DataType>string</DataType>
        <UserInputType>DropdownSingleSelect</UserInputType>
        <Restriction>
          <Enumeration Text="" Value="" SelectByDefault="true" />
          <Enumeration Text="Facebook" Value="facebook" SelectByDefault="false" />
          <Enumeration Text="Google" Value="google" SelectByDefault="false" />
          <Enumeration Text="LinkedIn" Value="linkedin" SelectByDefault="false" />
          <Enumeration Text="Magazine" Value="magazine" SelectByDefault="false" />
          <Enumeration Text="Podcast" Value="podcast" SelectByDefault="false" />
          <Enumeration Text="Other" Value="other" SelectByDefault="false" />
        </Restriction>
      </ClaimType>
    </ClaimsSchema>

    <ClaimsTransformations>
      <ClaimsTransformation Id="CreateDisplayName" TransformationMethod="FormatStringMultipleClaims">
        <InputClaims>
          <InputClaim ClaimTypeReferenceId="givenName" TransformationClaimType="inputClaim1"/>
          <InputClaim ClaimTypeReferenceId="surname" TransformationClaimType="inputClaim2"/>
        </InputClaims>
        <InputParameters>
          <InputParameter Id="stringFormat" DataType="string" Value="{0} {1}"/>
        </InputParameters>
        <OutputClaims>
          <OutputClaim ClaimTypeReferenceId="displayName" TransformationClaimType="outputClaim"/>
        </OutputClaims>
      </ClaimsTransformation>

      <ClaimsTransformation Id="CreateOtherMailsFromEmail" TransformationMethod="AddItemToStringCollection">
        <InputClaims>
          <InputClaim ClaimTypeReferenceId="signInEmail" TransformationClaimType="item" />
          <InputClaim ClaimTypeReferenceId="otherMails" TransformationClaimType="collection" />
        </InputClaims>
        <OutputClaims>
          <OutputClaim ClaimTypeReferenceId="otherMails" TransformationClaimType="collection" />
        </OutputClaims>
      </ClaimsTransformation>
 
      <ClaimsTransformation Id="CreateUserIdForMFA" TransformationMethod="FormatStringClaim">
        <InputClaims>
          <InputClaim ClaimTypeReferenceId="objectId" TransformationClaimType="inputClaim" />
        </InputClaims>
        <InputParameters>
          <InputParameter Id="stringFormat" DataType="string" Value="{0}@{RelyingPartyTenantId}" />
        </InputParameters>
        <OutputClaims>
          <OutputClaim ClaimTypeReferenceId="userIdForMFA" TransformationClaimType="outputClaim" />
        </OutputClaims>
      </ClaimsTransformation>
    </ClaimsTransformations>

    <ContentDefinitions>
      <ContentDefinition Id="content.generic">
        <LoadUri>#{Policy.ContentDefinition.Base}#</LoadUri>
        <RecoveryUri>#{Policy.ContentDefinition.Error}#</RecoveryUri>
        <DataUri>urn:com:microsoft:aad:b2c:elements:contract:selfasserted:1.2.0</DataUri>
      </ContentDefinition>
      <ContentDefinition Id="content.signin">
        <LoadUri>#{Policy.ContentDefinition.Unified}#</LoadUri>
        <RecoveryUri>#{Policy.ContentDefinition.Error}#</RecoveryUri>
        <DataUri>urn:com:microsoft:aad:b2c:elements:contract:unifiedssp:1.2.0</DataUri>
      </ContentDefinition>
      <ContentDefinition Id="content.signup">
        <LoadUri>#{Policy.ContentDefinition.SignUp}#</LoadUri>
        <RecoveryUri>#{Policy.ContentDefinition.Error}#</RecoveryUri>
        <DataUri>urn:com:microsoft:aad:b2c:elements:contract:selfasserted:1.2.0</DataUri>
      </ContentDefinition>
      <ContentDefinition Id="content.mfa">
        <LoadUri>#{Policy.ContentDefinition.Mfa}#</LoadUri>
        <RecoveryUri>#{Policy.ContentDefinition.Error}#</RecoveryUri>
        <DataUri>urn:com:microsoft:aad:b2c:elements:contract:multifactor:1.2.0</DataUri>
      </ContentDefinition>
    </ContentDefinitions>
  </BuildingBlocks>
  <ClaimsProviders>

    <!-- AAD Access -->

    <ClaimsProvider>
      <DisplayName>AAD</DisplayName>
      <TechnicalProfiles>

        <!-- Common -->

        <TechnicalProfile Id="AAD-Common">
          <DisplayName>Azure Active Directory</DisplayName>
          <Protocol Name="Proprietary" Handler="Web.TPEngine.Providers.AzureActiveDirectoryProvider, Web.TPEngine, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" />
          <Metadata>
            <Item Key="ApplicationObjectId">#{Policy.B2C.AAD.ApplicationObjectId}#</Item>
            <Item Key="ClientId">#{Policy.B2C.AAD.ClientId}#</Item>
          </Metadata>
          <CryptographicKeys>
            <Key Id="issuer_secret" StorageReferenceId="B2C_1A_TokenSigningKeyContainer" />
          </CryptographicKeys>
          <!-- We need this here to suppress the SelfAsserted provider from invoking SSO on validation profiles. -->
          <IncludeInSso>false</IncludeInSso>
          <UseTechnicalProfileForSessionManagement ReferenceId="SM-Noop"/>
        </TechnicalProfile>

        
        <TechnicalProfile Id="AAD-ReadUser-ById">
          <DisplayName>Read user by ID</DisplayName>
          <Metadata>
            <Item Key="Operation">Read</Item>
            <Item Key="RaiseErrorIfClaimsPrincipalDoesNotExist">false</Item>
          </Metadata>
          <InputClaims>
            <InputClaim ClaimTypeReferenceId="objectId" />
          </InputClaims>
          <OutputClaims>
            <OutputClaim ClaimTypeReferenceId="objectId" />
            <OutputClaim ClaimTypeReferenceId="userPrincipalName" />
            <OutputClaim ClaimTypeReferenceId="otherMails" />
            <OutputClaim ClaimTypeReferenceId="givenName" />
            <OutputClaim ClaimTypeReferenceId="surname" />
            <OutputClaim ClaimTypeReferenceId="displayName" />
            <OutputClaim ClaimTypeReferenceId="extension_referrer" />
            <OutputClaim ClaimTypeReferenceId="strongAuthenticationPhoneNumber" />
          </OutputClaims>
          <IncludeTechnicalProfile ReferenceId="AAD-Common"/>
        </TechnicalProfile>

        <TechnicalProfile Id="AAD-ReadUser-ByAlternativeSecurityId">
          <Metadata>
            <Item Key="Operation">Read</Item>
            <Item Key="RaiseErrorIfClaimsPrincipalDoesNotExist">false</Item>
          </Metadata>
          <InputClaims>
            <InputClaim ClaimTypeReferenceId="AlternativeSecurityId" PartnerClaimType="alternativeSecurityId" Required="true" />
          </InputClaims>
          <OutputClaims>  
            <OutputClaim ClaimTypeReferenceId="objectId" />
            <OutputClaim ClaimTypeReferenceId="userPrincipalName" />
            <OutputClaim ClaimTypeReferenceId="otherMails" />
            <OutputClaim ClaimTypeReferenceId="givenName" />
            <OutputClaim ClaimTypeReferenceId="surname" />
            <OutputClaim ClaimTypeReferenceId="displayName" />
            <OutputClaim ClaimTypeReferenceId="extension_referrer" />
          </OutputClaims>
          <IncludeTechnicalProfile ReferenceId="AAD-Common" />
        </TechnicalProfile>

      </TechnicalProfiles>
    </ClaimsProvider>

    <!-- MFA -->

    <ClaimsProvider>
      <DisplayName>Multifactor authentication</DisplayName>
      <TechnicalProfiles>
        <TechnicalProfile Id="UserAccount-PhoneMfa">
          <DisplayName>Phone MFA</DisplayName>
          <Protocol Name="Proprietary" Handler="Web.TPEngine.Providers.PhoneFactorProtocolProvider, Web.TPEngine, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" />
          <Metadata>
            <Item Key="ContentDefinitionReferenceId">content.mfa</Item>
            <Item Key="ManualPhoneNumberEntryAllowed">true</Item>
            <Item Key="language.intro_mixed">Please verify your identity using your phone</Item>
          </Metadata>
          <CryptographicKeys>
            <Key Id="issuer_secret" StorageReferenceId="B2C_1A_TokenSigningKeyContainer" />
          </CryptographicKeys>
          <InputClaimsTransformations>
            <InputClaimsTransformation ReferenceId="CreateUserIdForMFA" />
          </InputClaimsTransformations>
          <InputClaims>
            <InputClaim ClaimTypeReferenceId="userIdForMFA" PartnerClaimType="UserId" />
            <InputClaim ClaimTypeReferenceId="strongAuthenticationPhoneNumber" />
          </InputClaims>
          <OutputClaims>
            <OutputClaim ClaimTypeReferenceId="Verified.strongAuthenticationPhoneNumber" PartnerClaimType="Verified.OfficePhone" />
            <OutputClaim ClaimTypeReferenceId="loa" DefaultValue="2" AlwaysUseDefaultValue="true" />
          </OutputClaims>
          <UseTechnicalProfileForSessionManagement ReferenceId="SM-MFA" />
        </TechnicalProfile>
      </TechnicalProfiles>
    </ClaimsProvider>

    <!-- Errors -->

    <ClaimsProvider>
      <DisplayName>Errors</DisplayName>
      <TechnicalProfiles>
        <TechnicalProfile Id="SelfAsserted-Error">
          <DisplayName>Error message</DisplayName>
          <Protocol Name="Proprietary" Handler="Web.TPEngine.Providers.SelfAssertedAttributeProvider, Web.TPEngine, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null"/>
          <Metadata>
            <Item Key="ContentDefinitionReferenceId">content.generic</Item>
            <Item Key="setting.showContinueButton">false</Item>
            <Item Key="setting.showCancelButton">true</Item>
            <Item Key="language.intro">Sorry, an error occurred</Item>
          </Metadata>
          <InputClaims>
            <InputClaim ClaimTypeReferenceId="pageMessage"/>
          </InputClaims>
          <OutputClaims>
            <OutputClaim ClaimTypeReferenceId="pageMessage"/>
          </OutputClaims>
        </TechnicalProfile>
      </TechnicalProfiles>
    </ClaimsProvider>

    <!-- Session Management -->

    <ClaimsProvider>
      <DisplayName>Session Management</DisplayName>
      <TechnicalProfiles>
        <TechnicalProfile Id="SM-Noop">
          <DisplayName>Noop Session Management Provider</DisplayName>
          <Protocol Name="Proprietary" Handler="Web.TPEngine.SSO.NoopSSOSessionProvider, Web.TPEngine, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" />
        </TechnicalProfile>

        <TechnicalProfile Id="SM-AAD">
          <DisplayName>Session Management Provider</DisplayName>
          <Protocol Name="Proprietary" Handler="Web.TPEngine.SSO.DefaultSSOSessionProvider, Web.TPEngine, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" />
          <PersistedClaims>
            <PersistedClaim ClaimTypeReferenceId="objectId" />
            <PersistedClaim ClaimTypeReferenceId="idp" />
            <PersistedClaim ClaimTypeReferenceId="authenticationSource" />
            <PersistedClaim ClaimTypeReferenceId="loa" />
          </PersistedClaims>
          <OutputClaims>
            <OutputClaim ClaimTypeReferenceId="objectIdFromSession" DefaultValue="true"/>
          </OutputClaims>
        </TechnicalProfile>

        <TechnicalProfile Id="SM-SocialLogin">
          <DisplayName>Session Mananagement Provider</DisplayName>
          <Protocol Name="Proprietary" Handler="Web.TPEngine.SSO.ExternalLoginSSOSessionProvider, Web.TPEngine, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" />
          <Metadata>
            <Item Key="AlwaysFetchClaimsFromProvider">true</Item>
          </Metadata>
          <PersistedClaims>
            <PersistedClaim ClaimTypeReferenceId="AlternativeSecurityId" />
          </PersistedClaims>
        </TechnicalProfile>
 
        <TechnicalProfile Id="SM-MFA">
          <DisplayName>Session Mananagement Provider</DisplayName>
          <Protocol Name="Proprietary" Handler="Web.TPEngine.SSO.DefaultSSOSessionProvider, Web.TPEngine, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" />
          <PersistedClaims>
            <PersistedClaim ClaimTypeReferenceId="Verified.strongAuthenticationPhoneNumber" />
            <PersistedClaim ClaimTypeReferenceId="loa" />
          </PersistedClaims>
        </TechnicalProfile>
        
        <TechnicalProfile Id="SM-AAD-Read">
          <DisplayName>User Journey Context Provider</DisplayName>
          <Protocol Name="Proprietary" Handler="Web.TPEngine.Providers.UserJourneyContextProvider, Web.TPEngine, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" />
          <OutputClaims>
            <OutputClaim ClaimTypeReferenceId="objectId" />
            <OutputClaim ClaimTypeReferenceId="idp" />
            <OutputClaim ClaimTypeReferenceId="authenticationSource" />
            <OutputClaim ClaimTypeReferenceId="loa" />
          </OutputClaims>
          <UseTechnicalProfileForSessionManagement ReferenceId="SM-AAD" />
        </TechnicalProfile>

      </TechnicalProfiles>
    </ClaimsProvider>

  </ClaimsProviders>
</TrustFrameworkPolicy>