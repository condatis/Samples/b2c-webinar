﻿// <copyright file="ValidatePasswordResponse.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Models.Passwords
{
    using System.Collections.Generic;

    /// <summary>
    /// Provides a response to a password validation request.
    /// </summary>
    public class ValidatePasswordResponse
    {
        private List<string> validationErrors = new List<string>();

        /// <summary>
        /// Gets whether the validation passed.
        /// </summary>
        public bool IsValid => this.validationErrors.Count == 0;

        /// <summary>
        /// Gets a list of the errors raised during this validation.
        /// </summary>
        public IReadOnlyCollection<string> ValidationErrors => this.validationErrors.AsReadOnly();

        /// <summary>
        /// Adds a new error.
        /// </summary>
        /// <param name="message">Error message.</param>
        public void AddError(string message)
        {
            this.validationErrors.Add(message);
        }
    }
}
