﻿// <copyright file="HttpPolicyHandler.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Services.Http
{
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// Provides a way to apply a <see cref="HttpPolicies.Strategy"/> to a HTTP request.
    /// </summary>
    public class HttpPolicyHandler : DelegatingHandler
    {
        /// <inheritdoc />
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            return HttpPolicies.Strategy.ExecuteAsync(ct => base.SendAsync(request, ct), cancellationToken);
        }
    }
}