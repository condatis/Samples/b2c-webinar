﻿// <copyright file="CreateLegacyUserRequest.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Models.LegacyUsers
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Represents a request to create legacy user.
    /// </summary>
    public class CreateLegacyUserRequest
    {
        /// <summary>
        /// Gets or sets the user's email address.
        /// </summary>
        [Required]
        public string EmailAddress { get; set; }

        /// <summary>
        /// Gets or sets the user's password.
        /// </summary>
        [Required]
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the user's forename.
        /// </summary>
        [Required]
        public string Forename { get; set; }

        /// <summary>
        /// Gets or sets the user's surname.
        /// </summary>
        [Required]
        public string Surname { get; set; }
    }
}
