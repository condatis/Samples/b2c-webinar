﻿// <copyright file="UserGroupsResponse.cs" company="Sitekit Systems Ltd t/a Condatis">
// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Condatis.Webinars.B2C.Webinar4.Api.Models.Groups
{
    /// <summary>
    /// Response to an API call containing the groups assigned to a user.
    /// </summary>
    public class UserGroupsResponse
    {
        /// <summary>
        /// Gets or sets the unique ID of the user.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the groups assigned to the user.
        /// </summary>
        public string[] Groups { get; set; }
    }
}
